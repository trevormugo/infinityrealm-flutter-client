import 'package:flutter/material.dart';
import 'dart:math';

import 'package:line_icons/line_icons.dart';

import '../models/particlemodel.dart';

class BackgroundAnimation extends StatefulWidget {
  BackgroundAnimation({
    @required this.nooficons,
    @required this.width,
    @required this.height,
    @required this.shouldstart,
  });
  final int nooficons;
  final double width;
  final double height;
  final bool shouldstart;
  @override
  State createState() => BackgroundAnimationState();
}

class BackgroundAnimationState extends State<BackgroundAnimation>
    with SingleTickerProviderStateMixin {
  AnimationController backgroundanimationcontroller;
  Animation<Offset> _animation;
  Animation _opacityanimation;

  var random = new Random();
  List<Particle> iconparticles = [];
  double width;
  double height;
  @override
  void initState() {
    super.initState();
    width = widget.width;
    height = widget.height;

    List.generate(
      widget.nooficons,
      (index) => iconparticles.add(
        Particle(
          iconparticles: LineIcons.music,
          position: Offset(
            newRandom() * widget.width,
            newRandom() * widget.height,
          ),
          random: newRandom(),
        ),
      ),
    );
    backgroundanimationcontroller = AnimationController(
      vsync: this,
      duration: Duration(
        seconds: 32,
      ),
    );

    //if (widget.shouldstart == true) {
      backgroundanimationcontroller.forward();
    //}
  }

  @override
  void dispose() {
    super.dispose();
    backgroundanimationcontroller?.dispose();
  }

  double newRandom() {
    return random.nextDouble();
  }

  @override
  Widget build(BuildContext context) {
    _animation = Tween(
      begin: Offset(
        newRandom() * (widget.width), //* (2 / 3)),
        widget.height * (11 / 10),
      ),
      end: Offset(
        newRandom() * widget.width,
        -(widget.height * (11 / 10)),
      ),
    ).animate(
      CurvedAnimation(
        parent: backgroundanimationcontroller,
        curve: Curves.easeIn,
      ),
    );
    _opacityanimation = Tween(
      begin: 0.5,
      end: 0.0,
    ).animate(
      CurvedAnimation(
        parent: backgroundanimationcontroller,
        curve: Curves.easeIn,
      ),
    );
    backgroundanimationcontroller.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        print("complete");
        setState(() {
          iconparticles = iconparticles
              .map(
                (paricle) => Particle(
                  iconparticles: LineIcons.music,
                  position: Offset(
                    newRandom() * widget.width,
                    newRandom() * widget.height,
                  ),
                  random: newRandom(),
                ),
              )
              .toList();
        });
        backgroundanimationcontroller.reset();
        backgroundanimationcontroller.forward();
      }
    });
    return Positioned.fill(
      child: AnimatedBuilder(
        animation: backgroundanimationcontroller,
        builder: (context, child) => CustomPaint(
          painter: ParticlePainter(
            particles: iconparticles,
            width: widget.width,
            height: widget.height,
            progress: _animation.value,
            opacity: _opacityanimation.value,
          ),
        ),
      ),
    );
  }
}

class ParticlePainter extends CustomPainter {
  ParticlePainter(
      {@required this.particles,
      @required this.width,
      @required this.height,
      @required this.opacity,
      @required this.progress});
  List<Particle> particles;
  double width;
  double height;
  double opacity;
  Offset progress;
  @override
  void paint(Canvas canvas, Size size) {
    TextPainter textPainter = TextPainter(textDirection: TextDirection.rtl);
    particles.forEach((particle) {
      textPainter.text = TextSpan(
        text: String.fromCharCode(particle.iconparticles.codePoint),
        style: TextStyle(
          fontSize: (particle.random * width) * (1 / 3),
          fontFamily: particle.iconparticles.fontFamily,
          foreground: Paint()
            ..style = PaintingStyle.fill
            ..strokeWidth = 1
            ..color = Colors.black45.withOpacity(opacity),
        ),
      );
      textPainter.layout();
      textPainter.paint(
          canvas,
          Offset(
            particle.position.dx,
            progress.dy + particle.position.dy,
          ));
    });
    particles.forEach((particle) {
      textPainter.text = TextSpan(
        text: String.fromCharCode(particle.iconparticles.codePoint),
        style: TextStyle(
          fontSize: (particle.random * width) * (2 / 3),
          fontFamily: particle.iconparticles.fontFamily,
          foreground: Paint()
            ..style = PaintingStyle.stroke
            ..strokeWidth = 1
            ..color = Colors.black.withOpacity(opacity),
        ),
      );
      textPainter.layout();
      textPainter.paint(
          canvas,
          Offset(
            particle.position.dx,
            (progress.dy + particle.position.dy) +
                (progress.dy + particle.position.dy),
          ));
    });
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}
