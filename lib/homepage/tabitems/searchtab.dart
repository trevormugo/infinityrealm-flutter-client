import 'package:flutter/material.dart';

class SearchTab extends StatefulWidget {
  @override
  _SearchTabInstance createState() => _SearchTabInstance();
}

class _SearchTabInstance extends State<SearchTab> {
  List<String> searches = [];

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        builder: (BuildContext context, AsyncSnapshot snapshot) {
      searches = snapshot.data;
      if (snapshot.data == null) {
        return Center(
          child: CircularProgressIndicator(),
        );
      } else {
        return ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: searches.length,
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemBuilder: (context, index) {
            return ListTile(
              leading: Icon(Icons.format_list_bulleted),
              title: Text(searches[index]),
            );
          },
        );
      }
    });
  }
}
