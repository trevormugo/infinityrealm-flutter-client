import 'package:flutter/material.dart';

class ShelfTab extends StatefulWidget {
  @override
  _ShelfTabInstance createState() => _ShelfTabInstance();
}

class _ShelfTabInstance extends State<ShelfTab> {
  List<String> shelfs = [];

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        builder: (BuildContext context, AsyncSnapshot snapshot) {
      shelfs = snapshot.data;
      if (snapshot.data == null) {
        return Center(
          child: CircularProgressIndicator(),
        );
      } else {
        return ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: shelfs.length,
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemBuilder: (context, index) {
            return ListTile(
              leading: Icon(Icons.format_list_bulleted),
              title: Text(shelfs[index]),
            );
          },
        );
      }
    });
  }
}
