import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';

import '../../restapi.dart';
import '../../models/homeitems.dart';
import '../hometabcategories/curationobj.dart';
import '../hometabcategories/uploadsobj.dart';

class HomeTab extends StatefulWidget {
  HomeTab({
    @required this.userid,
    @required this.callback,
    @required this.navigatorKey,
  });
  final String userid;
  final Function callback;
  final GlobalKey<NavigatorState> navigatorKey;

  @override
  _HomeTabInstance createState() => _HomeTabInstance();
}

class _HomeTabInstance extends State<HomeTab> {
  List<HomeCategories> categories;

  @override
  void initState() {
    super.initState();
    initializepermissions();
  }

  void initializepermissions() async {
    LocationPermission checkpermission = await checkPermission();
    if (checkpermission == LocationPermission.whileInUse ||
        checkpermission == LocationPermission.always) {
      print("permissions granted");
    } else {
      LocationPermission requestpermission = await requestPermission();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      child: FutureBuilder(
        future: RestApi().fetchhomedata("/uploads/fetchhomedata"),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.data == null) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            print(snapshot.data);
            return ListView.builder(
              scrollDirection: Axis.vertical,
              itemCount: snapshot.data["items"].length,
              shrinkWrap: false,
              itemBuilder: (context, index) {
                return (snapshot.data["items"][index]["iscuratedobject"] ==
                        false)
                    ? Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.all(10),
                            child: Text(
                              snapshot.data["items"][index]["category"],
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w300,
                              ),
                            ),
                          ),
                          UploadsObj(
                            items: snapshot.data["items"][index]["uploads"],
                            footercallback: widget.callback,
                          ),
                        ],
                      )
                    : Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.all(10),
                            child: Text(
                              snapshot.data["items"][index]["category"],
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w300,
                              ),
                            ),
                          ),
                          CurationObj(
                            items: snapshot.data["items"][index]["curatedobj"],
                            type: snapshot.data["items"][index]["type"],
                            navigatorKey: widget.navigatorKey,
                          ),
                        ],
                      );
              },
            );
          }
        },
      ),
    );
  }
}
