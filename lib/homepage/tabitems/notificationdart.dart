import 'package:flutter/material.dart';

import '../../models/notificationsmodel.dart';

class NotificationsTab extends StatefulWidget {
  @override
  NotificationsTabInstance createState() => NotificationsTabInstance();
}

class NotificationsTabInstance extends State<NotificationsTab> {
  static List _notifications = [];

  static void addnotificationobject(NotificationReq notification) {
    _notifications.insert(0, notification);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        builder: (BuildContext context, AsyncSnapshot snapshot) {
      _notifications = snapshot.data;
      if (snapshot.data == null) {
        return Center(
          child: CircularProgressIndicator(),
        );
      } else {
        return ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: _notifications.length,
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemBuilder: (context, index) {
            return ListTile(
              leading: Icon(Icons.format_list_bulleted),
              title: Text(_notifications[index]),
            );
          },
        );
      }
    });
  }
}
