import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:infinityrealm/models/placemarkerrequest.dart';
import "package:latlong/latlong.dart" as latLng;
import 'package:line_icons/line_icons.dart';

import '../../appbar.dart';
import '../../restapi.dart';

class Maps extends StatefulWidget {
  Maps({@required this.id});
  final id;
  @override
  State createState() => _MapsState();
}

class _MapsState extends State<Maps> {
  bool entertapmode = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _newmarkermodetoggle() {
    entertapmode = !entertapmode;
  }

  void _onTap(latLng.LatLng details) {
    if (entertapmode == true) {
      print("tap anywhere on the map");
      Map<String, dynamic> request = {
        "Account": widget.id,
        "Longitude": details.longitude,
        "Latitude": details.latitude
      };
      SignalRSocket().placemarker(request);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBarOnly(
          titletext: "Events",
        ),
      ),
      extendBodyBehindAppBar: true,
      extendBody: true,
      body: FutureBuilder(
          future: RestApi()
              .fetchmarkers("/maps/fetchfollowingmarkers/" + widget.id),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.data == null) {
              return Center(
                child: CircularProgressIndicator(),
              );
            } else {
              return Container(
                color: Colors.black54,
                child: FlutterMap(
                  options: MapOptions(
                    center: latLng.LatLng(51.5, -0.09),
                    zoom: 13.0,
                    onTap: _onTap,
                  ),
                  /* layers: [
                  MarkerLayerOptions(
                    markers: [
                      Marker(
                        width: 80.0,
                        height: 80.0,
                        point: latLng.LatLng(51.5, -0.09),
                        builder: (ctx) => Container(
                          child: FlutterLogo(),
                        ),
                      ),
                    ],
                  ),
                ],*/
                  children: <Widget>[
                    TileLayerWidget(
                      options: TileLayerOptions(
                        urlTemplate:
                            "https://api.mapbox.com/styles/v1/trevor900trill/ckka53ph92zi117lfulbxxnp1/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoidHJldm9yOTAwdHJpbGwiLCJhIjoiY2sxY2Fnc3dmMDFneTNkbHBudzF6YjdsYyJ9.9YpcHcIAWA7iKeTsT4UDyQ",
                        additionalOptions: {
                          "accessToken":
                              "pk.eyJ1IjoidHJldm9yOTAwdHJpbGwiLCJhIjoiY2sxY2Fnc3dmMDFneTNkbHBudzF6YjdsYyJ9.9YpcHcIAWA7iKeTsT4UDyQ",
                          "id": "mapbox.mapbox-streets-v8",
                        },
                      ),
                    ),
                    MarkerLayerWidget(
                      options: MarkerLayerOptions(markers: [
                        Marker(
                          width: 80.0,
                          height: 80.0,
                          point: latLng.LatLng(51.5, -0.09),
                          builder: (ctx) => Container(
                            child: FlutterLogo(),
                          ),
                        ),
                      ]),
                    ),
                  ],
                ),
              );
            }
          }),
      floatingActionButtonLocation: FloatingActionButtonLocation.miniEndFloat,
      floatingActionButton: FloatingActionButton(
        onPressed: _newmarkermodetoggle,
        child: Icon(LineIcons.map_marker),
      ),
    );
  }
}
