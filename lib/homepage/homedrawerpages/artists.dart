import 'package:flutter/material.dart';

import '../../appbar.dart';
import '../../restapi.dart';

class Artists extends StatefulWidget {
  Artists({@required this.id});
  final id;

  @override
  State createState() => _ArtistsState();
}

class _ArtistsState extends State<Artists> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBarOnly(
          titletext: "Artists",
        ),
      ),
      extendBodyBehindAppBar: true,
      extendBody: true,
      body: FutureBuilder(
          future:
              RestApi().fetchuploadsbyartistid("/artists/uploads/" + widget.id),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.data == null) {
              return Center(
                child: CircularProgressIndicator(),
              );
            } else {
              return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(title: Text(snapshot.data[index][""]));
                  });
            }
          }),
    );
  }
}
