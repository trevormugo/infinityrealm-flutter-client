import 'package:flutter/material.dart';

import '../../appbar.dart';
import '../../restapi.dart';

class CuratedObjSaves extends StatefulWidget {
  CuratedObjSaves({
    @required this.curatedobjid,
    @required this.curatedobjname,
    @required this.callback,
  });
  final String curatedobjid;
  final String curatedobjname;
  final Function callback;

  @override
  State createState() => _CuratedObjSavesState();
}

class _CuratedObjSavesState extends State<CuratedObjSaves> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBarOnly(
          titletext: widget.curatedobjname,
        ),
      ),
      extendBodyBehindAppBar: true,
      extendBody: true,
      body: FutureBuilder(
          future: RestApi().fetchcuratedsavedobj(
              "/uploads/fetchcuratedsavedobj?id=${widget.curatedobjid}&type=playlist"),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.data == null) {
              return Center(
                child: CircularProgressIndicator(),
              );
            } else {
              var t = snapshot.data;
              print("VVVVVVVVVVVVVVVVVVVVVVVV $t ");
              return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(
                      onTap: () {
                        if (snapshot.data[index]['uploads'][0]
                                    ['musicfilemimetype'] ==
                                "video/avi" ||
                            snapshot.data[index]['uploads'][0]
                                    ['musicfilemimetype'] ==
                                "video/flv" ||
                            snapshot.data[index]['uploads'][0]
                                    ['musicfilemimetype'] ==
                                "video/mp4" ||
                            snapshot.data[index]['uploads'][0]
                                    ['musicfilemimetype'] ==
                                "video/mov" ||
                            snapshot.data[index]['uploads'][0]
                                    ['musicfilemimetype'] ==
                                "video/wmv" ||
                            snapshot.data[index]['uploads'][0]
                                    ['musicfilemimetype'] ==
                                "video/webm" ||
                            snapshot.data[index]['uploads'][0]
                                    ['musicfilemimetype'] ==
                                "video/webvtt" ||
                            snapshot.data[index]['uploads'][0]
                                    ['musicfilemimetype'] ==
                                "video/ogt") {
                          Map<String, dynamic> values = {
                            "id": snapshot.data[index]['uploads'][0]['id'],
                            "displayname": snapshot.data[index]['uploads'][0]
                                ['displayname'],
                            "uploader": snapshot.data[index]['uploads'][0]
                                ['uploader'],
                            "downloadable": snapshot.data[index]['uploads'][0]
                                ['downloadable'],
                            "downloads": snapshot.data[index]['uploads'][0]
                                ['downloads'],
                            "isprivate": snapshot.data[index]['uploads'][0]
                                ['isprivate'],
                            "views": snapshot.data[index]['uploads'][0]
                                ['views'],
                            "reported": snapshot.data[index]['uploads'][0]
                                ['reported'],
                            "timestamp": snapshot.data[index]['uploads'][0]
                                ['timestamp'],
                            "thumbnailpath": snapshot.data[index]['uploads'][0]
                                ['thumbnailpath'],
                            "thumbnailsize": snapshot.data[index]['uploads'][0]
                                ['thumbnailsize'],
                            "thumbnailmimetype": snapshot.data[index]['uploads']
                                [0]['thumbnailmimetype'],
                            "thumbnailext": snapshot.data[index]['uploads'][0]
                                ['thumbnailext'],
                            "musicfilepath": snapshot.data[index]['uploads'][0]
                                ['musicfilepath'],
                            "musicfilesize": snapshot.data[index]['uploads'][0]
                                ['musicfilesize'],
                            "musicfilemimetype": snapshot.data[index]['uploads']
                                [0]['musicfilemimetype'],
                            "musicfileext": snapshot.data[index]['uploads'][0]
                                ['musicfileext'],
                            "artistid": snapshot.data[index]['uploads'][0]
                                ['artistid'],
                          };

                          return widget.callback(values, "video",
                              "${snapshot.data[index]['uploads'][0]['id']}");
                        } else {
                          Map<String, dynamic> values = {
                            "id": snapshot.data[index]['uploads'][0]['id'],
                            "displayname": snapshot.data[index]['uploads'][0]
                                ['displayname'],
                            "uploader": snapshot.data[index]['uploads'][0]
                                ['uploader'],
                            "downloadable": snapshot.data[index]['uploads'][0]
                                ['downloadable'],
                            "downloads": snapshot.data[index]['uploads'][0]
                                ['downloads'],
                            "isprivate": snapshot.data[index]['uploads'][0]
                                ['isprivate'],
                            "views": snapshot.data[index]['uploads'][0]
                                ['views'],
                            "reported": snapshot.data[index]['uploads'][0]
                                ['reported'],
                            "timestamp": snapshot.data[index]['uploads'][0]
                                ['timestamp'],
                            "thumbnailpath": snapshot.data[index]['uploads'][0]
                                ['thumbnailpath'],
                            "thumbnailsize": snapshot.data[index]['uploads'][0]
                                ['thumbnailsize'],
                            "thumbnailmimetype": snapshot.data[index]['uploads']
                                [0]['thumbnailmimetype'],
                            "thumbnailext": snapshot.data[index]['uploads'][0]
                                ['thumbnailext'],
                            "musicfilepath": snapshot.data[index]['uploads'][0]
                                ['musicfilepath'],
                            "musicfilesize": snapshot.data[index]['uploads'][0]
                                ['musicfilesize'],
                            "musicfilemimetype": snapshot.data[index]['uploads']
                                [0]['musicfilemimetype'],
                            "musicfileext": snapshot.data[index]['uploads'][0]
                                ['musicfileext'],
                            "artistid": snapshot.data[index]['uploads'][0]
                                ['artistid'],
                          };
                          return widget.callback(values, "audio",
                              "${snapshot.data[index]['uploads'][0]['id']}");
                        }
                      },
                      title: Text(
                          snapshot.data[index]["uploads"][0]["displayname"]),
                    );
                  });
            }
          }),
    );
  }
}
