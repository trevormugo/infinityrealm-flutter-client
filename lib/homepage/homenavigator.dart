import 'package:flutter/material.dart';
import 'package:infinityrealm/models/crationpageargs.dart';

import 'curatedobjsaves/curateobjsaves.dart';
import 'homebackground.dart';
import 'homedrawerpages/artists.dart';
import 'homedrawerpages/merch.dart';
import 'homedrawerpages/podcasts.dart';

class HomeNavigator extends StatefulWidget {
  HomeNavigator({
    @required this.id,
    @required this.subid,
    @required this.navigatorKey,
    @required this.width,
    @required this.height,
    @required this.callback,
  });
  final String id;
  final String subid;
  final GlobalKey<NavigatorState> navigatorKey;
  final double width;
  final double height;
  final Function callback;

  @override
  _HomeNavigatorInstance createState() => _HomeNavigatorInstance();
}

class _HomeNavigatorInstance extends State<HomeNavigator> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => !await widget.navigatorKey.currentState.maybePop(),
      child: Navigator(
        key: widget.navigatorKey,
        initialRoute: "/",
        onGenerateRoute: (RouteSettings routeSettings) {
          if (routeSettings.name == "/") {
            return MaterialPageRoute(
                builder: (context) => HomePage(
                      userid: widget.id,
                      subid: widget.subid,
                      width: widget.width,
                      height: widget.height,
                      callback: widget.callback,
                      navigatorKey: widget.navigatorKey,
                    ));
          } else if (routeSettings.name == "/podcast") {
            return MaterialPageRoute(
              builder: (context) => Podcasts(
                id: widget.id,
              ),
            );
          } else if (routeSettings.name == "/artists") {
            return MaterialPageRoute(
              builder: (context) => Artists(
                id: widget.id,
              ),
            );
          } else if (routeSettings.name == "/merch") {
            return MaterialPageRoute(
              builder: (context) => Merch(
                id: widget.id,
              ),
            );
          } else if (routeSettings.name == "/curatedobjects") {
            final CurationPageArgs args = routeSettings.arguments;
            return MaterialPageRoute(
              builder: (context) => CuratedObjSaves(
                curatedobjid: args.curationid,
                curatedobjname: args.curationname,
                callback: widget.callback,
              ),
            );
          } else {
            return MaterialPageRoute(
              builder: (context) => Scaffold(
                body: Center(
                  child: Text("No defined routes"),
                ),
              ),
            );
          }
        },
      ),
    );
  }
}
