import 'package:flutter/material.dart';
import 'package:infinityrealm/homepage/homenavigator.dart';
import 'package:infinityrealm/players/audio/actuallaudioplayer.dart';
import 'package:infinityrealm/players/audio/audio.dart';
import 'package:infinityrealm/players/audio/fullscreenaudio.dart';
import 'package:infinityrealm/players/video/actuallvideotwo.dart';
import 'package:infinityrealm/players/video/fullscreenvideo.dart';
import 'package:infinityrealm/players/video/video.dart';
import 'package:infinityrealm/restapi.dart';
import 'package:video_player/video_player.dart';
import 'dart:math';

import '../ip.dart';
import './homebackground.dart';
import './homedrawer.dart';
import '../animations/backgroundanimation.dart';

class HomeRoot extends StatefulWidget {
  HomeRoot({@required this.subid, @required this.userid});
  final String subid;
  final String userid;
  @override
  State createState() => HomeRootState();
}

class HomeRootState extends State<HomeRoot> with TickerProviderStateMixin {
  AnimationController _animationcontroller;
  AnimationController _fullplayeranimationcontroller;
  AnimationController _animatedicon;

  Animation _animator;
  Animation _animatorwidthanimator;
  Animation _animatorradiusanimator;
  Animation _animatorxoffsetanimator;
  Animation _animatoropacityanimator;
  Animation _animatoropacityanimatorinverse;
  Animation _animatordiscanimator;

  Animation _fullplayerheightanimator;
  Animation _fullplayerwidthanimator;
  Animation _fullplayeropacityanimator;
  Animation _fullplayeropacityinverseanimator;
  Animation<AlignmentGeometry> _fullplayerscreenanimator;
  Animation _fullplayerscreenheightanimator;
  Animation _fullplayerscreenwidthanimator;
  Animation _fullplayerscreenthumbnailwidthanimator;
  Animation _fullplayerscreenthumbnaiheightanimator;
  Animation _fullplayerscreenvideowidthanimator;
  Animation _fullplayerscreenvideoheightanimator;

  double screenwidth;
  double screenheight;
  double positioned = kBottomNavigationBarHeight;

  List<IconData> iconparticles = [];
  bool heightanimationcomplete = false;
  AnimatedIconData icon = AnimatedIcons.pause_play;
  Widget currentfooterwidget = Container();

  final navigatorKey = GlobalKey<NavigatorState>();

  @override
  void initState() {
    super.initState();
    SignalRSocket().initconnection(widget.userid);

    _animationcontroller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 2000),
    );

    _fullplayeranimationcontroller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 2000),
    );

    _animator = CurvedAnimation(
      parent: _animationcontroller,
      curve: Curves.easeInOutQuad,
      reverseCurve: Curves.easeInQuad,
    );

    _animatorwidthanimator =
        Tween(begin: 1.0, end: 0.5).animate(CurvedAnimation(
      parent: _animationcontroller,
      curve: Curves.easeInOutQuad,
      reverseCurve: Curves.easeInQuad,
    ));

    _animatorradiusanimator =
        Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
      parent: _animationcontroller,
      curve: Curves.easeInOutQuad,
      reverseCurve: Curves.easeInQuad,
    ));

    _animatorxoffsetanimator =
        Tween(begin: 0.0, end: 0.6).animate(CurvedAnimation(
      parent: _animationcontroller,
      curve: Curves.easeInOutQuad,
      reverseCurve: Curves.easeInQuad,
    ));

    _animatoropacityanimator =
        Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
      parent: _animationcontroller,
      curve: Curves.easeInOutQuad,
      reverseCurve: Curves.easeInQuad,
    ));

    _animatoropacityanimatorinverse =
        Tween(begin: 1.0, end: 0.0).animate(CurvedAnimation(
      parent: _animationcontroller,
      curve: Curves.easeInOutQuad,
      reverseCurve: Curves.easeInQuad,
    ));

    _animatordiscanimator =
        Tween(begin: 0.0, end: 80.0).animate(CurvedAnimation(
      parent: _animationcontroller,
      curve: Curves.easeInOutQuad,
      reverseCurve: Curves.easeInQuad,
    ));

    _fullplayerheightanimator =
        Tween(begin: 0.08, end: 1.0).animate(CurvedAnimation(
      parent: _fullplayeranimationcontroller,
      curve: Curves.easeInOutQuad,
      reverseCurve: Curves.easeInQuad,
    ));

    _fullplayerwidthanimator =
        Tween(begin: 0.9, end: 1.0).animate(CurvedAnimation(
      parent: _fullplayeranimationcontroller,
      curve: Curves.easeInOutQuad,
      reverseCurve: Curves.easeInQuad,
    ));

    _fullplayeropacityanimator =
        Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
      parent: _fullplayeranimationcontroller,
      curve: Curves.easeInOutQuad,
      reverseCurve: Curves.easeInQuad,
    ));

    _fullplayeropacityinverseanimator =
        Tween(begin: 0.7, end: 0.0).animate(CurvedAnimation(
      parent: _fullplayeranimationcontroller,
      curve: Curves.easeInOutQuad,
      reverseCurve: Curves.easeInQuad,
    ));

    _fullplayerscreenanimator = Tween(
      begin: Alignment.bottomLeft,
      end: Alignment.center,
    ).animate(CurvedAnimation(
      parent: _fullplayeranimationcontroller,
      curve: Curves.easeInOutQuad,
      reverseCurve: Curves.easeInQuad,
    ));

    _fullplayerscreenheightanimator =
        Tween(begin: 0.1, end: 0.5).animate(CurvedAnimation(
      parent: _fullplayeranimationcontroller,
      curve: Curves.easeInOutQuad,
      reverseCurve: Curves.easeInQuad,
    ));

    _fullplayerscreenwidthanimator =
        Tween(begin: 0.4, end: 1.0).animate(CurvedAnimation(
      parent: _fullplayeranimationcontroller,
      curve: Curves.easeInOutQuad,
      reverseCurve: Curves.easeInQuad,
    ));

    _fullplayerscreenthumbnailwidthanimator =
        Tween(begin: 0.3, end: 1.0).animate(CurvedAnimation(
      parent: _fullplayeranimationcontroller,
      curve: Curves.easeInOutQuad,
      reverseCurve: Curves.easeInQuad,
    ));

    _fullplayerscreenthumbnaiheightanimator =
        Tween(begin: 0.4, end: 1.0).animate(CurvedAnimation(
      parent: _fullplayeranimationcontroller,
      curve: Curves.easeInOutQuad,
      reverseCurve: Curves.easeInQuad,
    ));

    _fullplayerscreenvideowidthanimator =
        Tween(begin: 0.3, end: 1.0).animate(CurvedAnimation(
      parent: _fullplayeranimationcontroller,
      curve: Curves.easeInOutQuad,
      reverseCurve: Curves.easeInQuad,
    ));

    _fullplayerscreenvideoheightanimator =
        Tween(begin: 0.4, end: 1.0).animate(CurvedAnimation(
      parent: _fullplayeranimationcontroller,
      curve: Curves.easeInOutQuad,
      reverseCurve: Curves.easeInQuad,
    ));
  }

  @override
  void dispose() {
    super.dispose();
    _animationcontroller?.dispose();
    _fullplayeranimationcontroller?.dispose();
  }

  void navigatetomaps(Widget destination) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => destination),
    );
  }

  void navigatesto(String route) {
    _animationcontroller.reverse();
    Navigator.of(navigatorKey.currentContext).pushNamed(route);
  }

  void _animatepauseplay() {
    if (!VideoAppState.playerController.value.isPlaying) {
      _animatedicon.reverse();
      VideoAppState.playerController.play();
    } else {
      _animatedicon.forward();
      VideoAppState.playerController.pause();
    }
  }

  Widget createWidget(String type, Map<String, dynamic> currentplayervalues) {
    if (type == "video") {
      return GestureDetector(
        onVerticalDragEnd: _onVerticalDragEnd,
        onVerticalDragUpdate: _onVerticalDragUpdate,
        child: AnimatedBuilder(
          animation: _animationcontroller,
          child: AnimatedBuilder(
            animation: _fullplayeranimationcontroller,
            builder: (context, child) => Container(
              child: Stack(
                overflow: Overflow.clip,
                alignment: AlignmentDirectional.bottomCenter,
                children: [
                  FadeTransition(
                    opacity: _fullplayeropacityinverseanimator,
                    child: Container(
                      height: screenheight * 0.1,
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Container(
                          width: screenwidth * 0.6,
                          child: VideoPlayerFoot(
                            videoname: currentplayervalues['displayname'],
                          ),
                        ),
                      ),
                    ),
                  ),
                  AlignTransition(
                    alignment: _fullplayerscreenanimator,
                    child: Stack(
                      children: <Widget>[
                        Container(
                          height: _fullplayerscreenvideoheightanimator.value *
                              screenheight,
                          width: _fullplayerscreenvideowidthanimator.value *
                              screenwidth,
                          child: VideoApp(
                            srcid: currentplayervalues['id'],
                          ),
                        ),
                        
                        /*FadeTransition(
                          opacity: _fullplayeropacityanimator,
                          child: Container(
                            child: FullScreenVideoPlayer(
                              screenheight: screenheight,
                              screenwidth: screenwidth,
                              songname: currentplayervalues['displayname'],
                              id: currentplayervalues['id'],
                            ),
                            width: screenwidth,
                            height: screenheight,
                          ),
                        ),*/
                      ],
                    ),
                  ),
                  /* FadeTransition(
                    opacity: _fullplayeropacityinverseanimator,
                    child: VideoProgressIndicator(
                      VideoAppState.playerController,
                      //allowScrubbing: true,
                    ),
                  ),*/
                ],
              ),
              width: _fullplayerwidthanimator.value * screenwidth,
              height: _fullplayerheightanimator.value * screenheight,
              decoration: BoxDecoration(
                color: Colors.black
                    .withOpacity(_fullplayeropacityinverseanimator.value),
              ),
            ),
          ),
          builder: (context, child) => Transform.translate(
            offset: Offset(_animatorxoffsetanimator.value * screenwidth, 0),
            child: Container(
              child: child,
              width: _animatorwidthanimator.value * screenwidth,
              decoration: BoxDecoration(
                color: Colors.black,
              ),
            ),
          ),
        ),
      );
    } else if (type == "audio") {
      return GestureDetector(
        onVerticalDragEnd: _onVerticalDragEnd,
        onVerticalDragUpdate: _onVerticalDragUpdate,
        child: AnimatedBuilder(
          animation: _animationcontroller,
          child: AnimatedBuilder(
            animation: _fullplayeranimationcontroller,
            builder: (context, child) => Container(
              child: Stack(
                overflow: Overflow.clip,
                alignment: AlignmentDirectional.bottomCenter,
                children: [
                  FadeTransition(
                    opacity: _fullplayeropacityinverseanimator,
                    child: Container(
                      height: screenheight * 0.1,
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Container(
                          width: screenwidth * 0.6,
                          child: AudioPlayerFoot(
                            audioname: currentplayervalues['displayname'],
                          ),
                        ),
                      ),
                    ),
                  ),
                  AlignTransition(
                    alignment: _fullplayerscreenanimator,
                    child: Stack(
                      children: <Widget>[
                        Container(
                          height:
                              _fullplayerscreenthumbnaiheightanimator.value *
                                  screenheight,
                          width: _fullplayerscreenthumbnailwidthanimator.value *
                              screenwidth,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                                image: NetworkImage(Adress.myip +
                                    "/uploads/fetchuploadthumbnail?id=${currentplayervalues['id']}&iscuratedobj=false"),
                                fit: BoxFit.cover),
                          ),
                        ),
                        FadeTransition(
                          opacity: _fullplayeropacityanimator,
                          child: Container(
                            child: FullScreenAudioPlayer(
                              screenheight: screenheight,
                              screenwidth: screenwidth,
                              songname: currentplayervalues['displayname'],
                              id: currentplayervalues['id'],
                            ),
                            //color: Colors.black
                            //    .withOpacity(_fullplayeropacityanimator.value),
                            width: screenwidth,
                            height: screenheight,
                          ),
                        ),
                      ],
                    ),
                  ),
                  FadeTransition(
                    opacity: _fullplayeropacityinverseanimator,
                    child: ActuallAudio(
                      srcid: currentplayervalues['id'],
                    ),
                  ),
                ],
              ),
              width: _fullplayerwidthanimator.value * screenwidth,
              height: _fullplayerheightanimator.value * screenheight,
              decoration: BoxDecoration(
                color: Colors.black
                    .withOpacity(_fullplayeropacityinverseanimator.value),
              ),
            ),
          ),
          builder: (context, child) => Transform.translate(
            offset: Offset(_animatorxoffsetanimator.value * screenwidth, 0),
            child: Container(
              child: Stack(
                alignment: AlignmentDirectional.center,
                children: [
                  FadeTransition(
                    opacity: _animatoropacityanimator,
                    child: ClipOval(
                      child: Container(
                        height: _animatordiscanimator.value,
                        width: _animatordiscanimator.value,
                        color: Colors.pink,
                        child: Center(
                          child: Text("disc"),
                        ),
                      ),
                    ),
                  ),
                  FadeTransition(
                    opacity: _animatoropacityanimatorinverse,
                    child: child,
                  ),
                ],
              ),
              width: _animatorwidthanimator.value * screenwidth,
              decoration: BoxDecoration(
                borderRadius:
                    BorderRadius.circular(_animatorradiusanimator.value),
              ),
            ),
          ),
        ),
      );
    } else {
      return Container();
    }
  }

  String typeofwidget;
  void footercallback(Map<String, dynamic> currentplayervalues,
      String uploadtype, String uploadid) {
    if (typeofwidget == "video" && uploadtype == "video") {
      VideoAppState.updateplayer(
          Adress.myip + "/uploads/streamuploadfile/" + uploadid);
      typeofwidget = uploadtype;
      setState(() {
        currentfooterwidget = createWidget(uploadtype, currentplayervalues);
      });
    } else if (typeofwidget == "audio" && uploadtype == "audio") {
      AudioInstance.updateplayer(uploadid);
      typeofwidget = uploadtype;
      setState(() {
        currentfooterwidget = createWidget(uploadtype, currentplayervalues);
      });
    } else {
      //if there was no widget initialized prior or if the widget to be initialized is not the same as the widget prior
      typeofwidget = uploadtype;
      setState(() {
        currentfooterwidget = createWidget(uploadtype, currentplayervalues);
      });
    }
  }

  void _onHorizontalDragStart(DragStartDetails details) {
    print(details.globalPosition);
  }

  void _onHorizontalDragEnd(DragEndDetails details) {
    if (_animationcontroller.value > 0.5) {
      _animationcontroller.forward();
      //BackgroundAnimationState().backgroundanimationcontroller.forward();
    } else {
      _animationcontroller.reverse();
    }
  }

  void _onHorizontalDragUpdate(DragUpdateDetails details) {
    var fractiondragged = details.primaryDelta / screenwidth;
    _animationcontroller.value =
        _animationcontroller.value + (3 * fractiondragged);
    print("${_animationcontroller.value}");
  }

  void _onVerticalDragEnd(DragEndDetails details) {
    if (_fullplayeranimationcontroller.value > 0.5) {
      _fullplayeranimationcontroller.forward();
      setState(() {
        heightanimationcomplete = true;
      });
    } else {
      _fullplayeranimationcontroller.reverse();
      setState(() {
        heightanimationcomplete = false;
      });
    }
  }

  void _onVerticalDragUpdate(DragUpdateDetails details) {
    var fractiondragged = details.primaryDelta / screenheight;
    _fullplayeranimationcontroller.value =
        _fullplayeranimationcontroller.value - (3 * fractiondragged);
    print(
        "ANIMATOOOOOOOOOOOR ------------ ${_fullplayeranimationcontroller.value} , ${fractiondragged} , ");
  }

  @override
  Widget build(BuildContext context) {
    screenwidth = MediaQuery.of(context).size.width;
    screenheight = MediaQuery.of(context).size.height;
    return Scaffold(
      extendBodyBehindAppBar: true,
      extendBody: true,
      body: GestureDetector(
        behavior: HitTestBehavior.deferToChild,
        onHorizontalDragStart: _onHorizontalDragStart,
        onHorizontalDragEnd: _onHorizontalDragEnd,
        onHorizontalDragUpdate: _onHorizontalDragUpdate,
        child: Stack(
          alignment: AlignmentDirectional.centerStart,
          overflow: Overflow.visible,
          children: <Widget>[
            //BACK PAINT
            Container(
              color: Colors.black38,
            ),

            //INVERTED TEXT
            Positioned(
              left: screenwidth * (3 / 5),
              child: Container(
                width: screenwidth * (2 / 5),
                height: screenheight,
                child: Stack(
                  overflow: Overflow.visible,
                  children: [
                    //Background animations
                    BackgroundAnimation(
                      nooficons: 15,
                      width: screenwidth * (2 / 5),
                      height: screenheight,
                      shouldstart: false,
                    ),
                    Center(
                      child: AnimatedBuilder(
                        animation: _animationcontroller,
                        builder: (context, child) => Container(
                          //color: Colors.black.withOpacity(_animator.value),
                          child: Transform.rotate(
                            angle: -(pi / 2),
                            alignment: Alignment.center,
                            child: child,
                          ),
                        ),
                        child: Text(
                          "INFINITE",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 30,
                            fontFamily: "Monoton",
                            color: Colors.black54,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),

            //HOMEPAGE TABS
            Positioned.fill(
              child: AnimatedBuilder(
                animation: _animationcontroller,
                child: HomeNavigator(
                  id: widget.userid,
                  subid: widget.subid,
                  width: screenwidth,
                  height: screenheight,
                  navigatorKey: navigatorKey,
                  callback: footercallback,
                ),
                builder: (context, child) => Transform.translate(
                  offset: Offset((_animator.value * screenwidth) * 3 / 5, 0),
                  child: Transform(
                    alignment: Alignment.centerLeft,
                    transform: Matrix4.identity()
                      ..setEntry(3, 2, 0.001)
                      ..rotateY((pi / 2 + 0.1) * -_animator.value),
                    child: child,
                  ),
                ),
              ),
            ),

            //DRAWER
            AnimatedBuilder(
              animation: _animationcontroller,
              child: HomeDrawer(
                id: widget.userid,
                width: screenwidth * (3 / 5),
                height: screenheight,
                navigate: navigatesto,
                navigatetomaps: navigatetomaps,
              ),
              builder: (context, child) => Transform.translate(
                offset:
                    Offset(((_animator.value - 1) * screenwidth) * 3 / 5, 0),
                child: Transform(
                  alignment: Alignment.centerRight,
                  transform: Matrix4.identity()
                    ..setEntry(3, 2, 0.001)
                    ..rotateY(pi * (1 - _animator.value) / 2),
                  child: child,
                ),
              ),
            ),

            //FOOTER PLAYER
            AnimatedPositioned(
              duration: Duration(milliseconds: 800),
              bottom: (heightanimationcomplete)
                  ? 0
                  : kBottomNavigationBarHeight + 5,
              child: currentfooterwidget,
            ),
          ],
        ),
      ),
    );
  }
}
