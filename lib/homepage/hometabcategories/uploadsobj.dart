import 'package:flutter/material.dart';
import 'package:cache_image/cache_image.dart';

import '../../ip.dart';

class UploadsObj extends StatefulWidget {
  UploadsObj({
    @required this.items,
    @required this.footercallback,
  });
  final items;
  final Function footercallback;
  @override
  _UploadsObjInstance createState() => _UploadsObjInstance();
}

class _UploadsObjInstance extends State<UploadsObj> {
  double screenwidth;
  double screenheight;

  void _ontap(int index) {
    if (widget.items[index]['uploads'][0]['musicfilemimetype'] == "video/avi" ||
        widget.items[index]['uploads'][0]['musicfilemimetype'] == "video/flv" ||
        widget.items[index]['uploads'][0]['musicfilemimetype'] == "video/mp4" ||
        widget.items[index]['uploads'][0]['musicfilemimetype'] == "video/mov" ||
        widget.items[index]['uploads'][0]['musicfilemimetype'] == "video/wmv" ||
        widget.items[index]['uploads'][0]['musicfilemimetype'] ==
            "video/webm" ||
        widget.items[index]['uploads'][0]['musicfilemimetype'] ==
            "video/webvtt" ||
        widget.items[index]['uploads'][0]['musicfilemimetype'] == "video/ogt") {
      Map<String, dynamic> values = {
        "id": widget.items[index]['uploads'][0]['id'],
        "displayname": widget.items[index]['uploads'][0]['displayname'],
        "uploader": widget.items[index]['uploads'][0]['uploader'],
        "downloadable": widget.items[index]['uploads'][0]['downloadable'],
        "downloads": widget.items[index]['uploads'][0]['downloads'],
        "isprivate": widget.items[index]['uploads'][0]['isprivate'],
        "views": widget.items[index]['uploads'][0]['views'],
        "reported": widget.items[index]['uploads'][0]['reported'],
        "timestamp": widget.items[index]['uploads'][0]['timestamp'],
        "thumbnailpath": widget.items[index]['uploads'][0]['thumbnailpath'],
        "thumbnailsize": widget.items[index]['uploads'][0]['thumbnailsize'],
        "thumbnailmimetype": widget.items[index]['uploads'][0]
            ['thumbnailmimetype'],
        "thumbnailext": widget.items[index]['uploads'][0]['thumbnailext'],
        "musicfilepath": widget.items[index]['uploads'][0]['musicfilepath'],
        "musicfilesize": widget.items[index]['uploads'][0]['musicfilesize'],
        "musicfilemimetype": widget.items[index]['uploads'][0]
            ['musicfilemimetype'],
        "musicfileext": widget.items[index]['uploads'][0]['musicfileext'],
        "artistid": widget.items[index]['uploads'][0]['artistid'],
      };

      return widget.footercallback(
          values, "audio", "${widget.items[index]['uploads'][0]['id']}");
    } else {
      Map<String, dynamic> values = {
        "id": widget.items[index]['uploads'][0]['id'],
        "displayname": widget.items[index]['uploads'][0]['displayname'],
        "uploader": widget.items[index]['uploads'][0]['uploader'],
        "downloadable": widget.items[index]['uploads'][0]['downloadable'],
        "downloads": widget.items[index]['uploads'][0]['downloads'],
        "isprivate": widget.items[index]['uploads'][0]['isprivate'],
        "views": widget.items[index]['uploads'][0]['views'],
        "reported": widget.items[index]['uploads'][0]['reported'],
        "timestamp": widget.items[index]['uploads'][0]['timestamp'],
        "thumbnailpath": widget.items[index]['uploads'][0]['thumbnailpath'],
        "thumbnailsize": widget.items[index]['uploads'][0]['thumbnailsize'],
        "thumbnailmimetype": widget.items[index]['uploads'][0]
            ['thumbnailmimetype'],
        "thumbnailext": widget.items[index]['uploads'][0]['thumbnailext'],
        "musicfilepath": widget.items[index]['uploads'][0]['musicfilepath'],
        "musicfilesize": widget.items[index]['uploads'][0]['musicfilesize'],
        "musicfilemimetype": widget.items[index]['uploads'][0]
            ['musicfilemimetype'],
        "musicfileext": widget.items[index]['uploads'][0]['musicfileext'],
        "artistid": widget.items[index]['uploads'][0]['artistid'],
      };
      return widget.footercallback(
          values, "video", "${widget.items[index]['uploads'][0]['id']}");
    }
  }

  @override
  Widget build(BuildContext context) {
    screenwidth = MediaQuery.of(context).size.width;
    screenheight = MediaQuery.of(context).size.height;
    return Container(
      height: screenheight * (1 / 3),
      width: screenwidth,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: widget.items.length,
        shrinkWrap: true,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () {
              _ontap(index);
            },
            child: Container(
              width: screenwidth * (7 / 10),
              height: (screenheight * (1 / 3)) * (4 / 5),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    width: (screenwidth * (7 / 10)) * (4 / 5),
                    height: ((screenheight * (1 / 3)) * (4 / 5)) * (7 / 10),
                    color: Colors.black.withOpacity(0.2),
                    margin: EdgeInsets.all(10),
                    child: Center(
                      child: Image(
                        fit: BoxFit.contain,
                        image: NetworkImage(Adress.myip +
                            "/uploads/fetchuploadthumbnail?id=${widget.items[index]['uploads'][0]['id']}&iscuratedobj=false"),
                      ),
                    ),
                  ),
                  Text(widget.items[index]["uploads"][0]["displayname"]),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
