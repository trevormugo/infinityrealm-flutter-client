import 'package:flutter/material.dart';
import 'package:cache_image/cache_image.dart';
import 'package:infinityrealm/models/crationpageargs.dart';
import '../../ip.dart';

class CurationObj extends StatefulWidget {
  CurationObj({
    @required this.items,
    @required this.type,
    @required this.navigatorKey,
  });
  final items;
  final String type;
  final GlobalKey<NavigatorState> navigatorKey;

  @override
  _CurationObjInstance createState() => _CurationObjInstance();
}

class _CurationObjInstance extends State<CurationObj> {
  double screenwidth;
  double screenheight;

  void _ontap(index) {
    Navigator.of(widget.navigatorKey.currentContext).pushNamed(
      "/curatedobjects",
      arguments: CurationPageArgs(
          curationid: widget.items[index]['curationobj'][0]['id'],
          curationname: widget.items[index]['curationobj'][0]['name']),
    );
  }

  @override
  Widget build(BuildContext context) {
    screenwidth = MediaQuery.of(context).size.width;
    screenheight = MediaQuery.of(context).size.height;
    return Container(
      height: screenheight * (1 / 3),
      width: screenwidth,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: widget.items.length,
        shrinkWrap: true,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () {
              _ontap(index);
            },
            child: Container(
             // width: screenwidth * (7 / 10),
             // height: (screenheight * (1 / 3)) * (4 / 5),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: (screenwidth * (7 / 10)) * (4 / 5),
                    height: ((screenheight * (1 / 3)) * (4 / 5)) * (7 / 10),
                    color: Colors.black.withOpacity(0.2),
                    margin: EdgeInsets.all(10),
                    child: Center(
                      child: Image(
                        fit: BoxFit.contain,
                        image: NetworkImage(Adress.myip +
                            "/uploads/fetchuploadthumbnail?id=${widget.items[index]['curationobj'][0]['id']}&iscuratedobj=true&type=${widget.type}"),
                      ),
                    ),
                  ),
                  Text(widget.items[index]["curationobj"][0]["name"]),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
