import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';

import 'tabitems/hometab.dart';
import 'tabitems/notificationdart.dart';
import 'tabitems/searchtab.dart';
import 'tabitems/shelftab.dart';

class HomePage extends StatefulWidget {
  HomePage({
    @required this.subid,
    @required this.userid,
    @required this.width,
    @required this.height,
    @required this.callback,
    @required this.navigatorKey,
  });
  final String subid;
  final String userid;
  final width;
  final height;
  final Function callback;
  final GlobalKey<NavigatorState> navigatorKey;

  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  List<Widget> _children;
  @override
  void initState() {
    super.initState();
    _children = [
      HomeTab(
        userid: widget.userid,
        callback: widget.callback,
        navigatorKey: widget.navigatorKey,
      ),
      SearchTab(),
      NotificationsTab(),
      ShelfTab()
    ];
  }

  int _currentindex = 0;

  void _navigate(int index) {
    setState(() {
      _currentindex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    void move(Widget destination) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => destination),
      );
    }

    return SafeArea(
      top: true,
      bottom: false,
      left: false,
      right: false,
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(60),
          child: AppBar(
            centerTitle: true,
            leading: Icon(LineIcons.bars),
            title: Text(
              "INFINITE",
              style: TextStyle(fontSize: 20, fontFamily: "Monoton"),
            ),
            actions: [CircleAvatar()],
          ),
        ),
        body: LayoutBuilder(builder: (context, constraints) {
          return SingleChildScrollView(
            child: ConstrainedBox(
              constraints: BoxConstraints(
                  minWidth: constraints.maxWidth,
                  minHeight: constraints.maxHeight),
              child: _children[_currentindex],
            ),
          );
        }),
        extendBodyBehindAppBar: true,
        extendBody: true,
        bottomNavigationBar: BottomNavigationBar(
          selectedItemColor: Colors.green,
          currentIndex: _currentindex,
          onTap: _navigate,
          items: [
            BottomNavigationBarItem(
              icon: Icon(LineIcons.home),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: Icon(LineIcons.search),
              label: 'Search',
            ),
            BottomNavigationBarItem(
              icon: Icon(LineIcons.bell),
              label: 'Notifications',
            ),
            BottomNavigationBarItem(
              icon: Icon(LineIcons.music),
              label: 'Shelf',
            ),
          ],
        ),
      ),
    );
  }
}
