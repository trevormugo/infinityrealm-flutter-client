import 'package:flutter/material.dart';
import 'package:infinityrealm/homepage/homedrawerpages/artists.dart';
import 'package:infinityrealm/homepage/homedrawerpages/merch.dart';
import 'package:infinityrealm/homepage/homedrawerpages/podcasts.dart';

import "./homebackground.dart";
import 'homedrawerpages/maps.dart';

class HomeDrawer extends StatefulWidget {
  HomeDrawer({
    @required this.width,
    @required this.height,
    @required this.id,
    @required this.navigate,
    @required this.navigatetomaps,
  });
  final String id;
  final double width;
  final double height;
  final Function navigate;
  final Function navigatetomaps;
  @override
  State createState() => HomeDrawerState();
}

class HomeDrawerState extends State<HomeDrawer> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black54.withOpacity(0.7),
      width: widget.width,
      height: widget.height,
      child: SafeArea(
        top: true,
        bottom: false,
        left: false,
        right: false,
        child: LayoutBuilder(builder: (context, constraints) {
          return SingleChildScrollView(
            child: ConstrainedBox(
              constraints: BoxConstraints(
                  minWidth: constraints.maxWidth,
                  minHeight: constraints.maxHeight),
              child: Padding(
                padding: EdgeInsets.all(20.0),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "MENU",
                      style: TextStyle(
                        fontFamily: "Monoton",
                        fontSize: 25,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        InkWell(
                          child: Text(
                            "MAPS",
                            style: TextStyle(
                              fontSize: 25,
                              fontWeight: FontWeight.w900,
                            ),
                          ),
                          onTap: () {
                            widget.navigatetomaps(
                              Maps(
                                id: widget.id,
                              ),
                            );
                          },
                        ),
                        InkWell(
                          child: Text(
                            "PODCAST",
                            style: TextStyle(
                              fontSize: 25,
                              fontWeight: FontWeight.w900,
                            ),
                          ),
                          onTap: () {
                            widget.navigate('/podcast');
                          },
                        ),
                        InkWell(
                          child: Text(
                            "ARTISTS",
                            style: TextStyle(
                              fontSize: 25,
                              fontWeight: FontWeight.w900,
                            ),
                          ),
                          onTap: () {
                            widget.navigate('/artists');
                          },
                        ),
                        InkWell(
                          child: Text(
                            "MERCH",
                            style: TextStyle(
                              fontSize: 25,
                              fontWeight: FontWeight.w900,
                            ),
                          ),
                          onTap: () {
                            widget.navigate('/merch');
                          },
                        ),
                      ],
                    ),
                    Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "ABOUT",
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w900,
                          ),
                        ),
                        Text(
                          "SUPPORT",
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w900,
                          ),
                        ),
                        Text(
                          "TERMS",
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w900,
                          ),
                        ),
                        Text(
                          "FAQS",
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w900,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          );
        }),
      ),
    );
  }
}
