import 'package:flutter/material.dart';

class AppBarOnly extends StatelessWidget {
  AppBarOnly({@required this.titletext});
  final String titletext;
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(titletext,
          style: TextStyle(color: Colors.white, fontFamily: 'NanumGothic'),
          textAlign: TextAlign.center),
      backgroundColor: Colors.black.withOpacity(0.5),
    );
  }
}
