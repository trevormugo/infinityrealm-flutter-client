import 'package:flutter/material.dart';

class MarkerRequest {
  String account;
  double longitude;
  double latitude;

  MarkerRequest({
    @required this.account,
    @required this.longitude,
    @required this.latitude,
  });
}
