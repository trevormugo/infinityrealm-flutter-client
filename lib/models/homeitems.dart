class HomeFetchResponse {
  List<HomeCategories> items;
  HomeFetchResponse({
    this.items,
  });

  factory HomeFetchResponse.fromJson(Map<String, dynamic> json) {
    return HomeFetchResponse(
      items: json['items'],
    );
  }
}

class HomeCategories {
  String category;
  bool iscuretedobj;
  List<HomeCuratedObj> curatedobj;
  List<HomeUploadItemObj> uploads;

  HomeCategories({
    this.category,
    this.iscuretedobj,
    this.curatedobj,
    this.uploads,
  });

  factory HomeCategories.fromJson(Map<String, dynamic> json) {
    return HomeCategories(
      category: json['category'],
      iscuretedobj: json['iscuretedobj'],
      curatedobj: json['curatedobj'],
      uploads: json['uploads'],
    );
  }
}

class HomeCuratedObj {
  Map id;
  String groupedobjectid;
  int noofstreams;
  List<HomeCuration> curatedobject;

  HomeCuratedObj({
    this.id,
    this.groupedobjectid,
    this.noofstreams,
    this.curatedobject,
  });

  factory HomeCuratedObj.fromJson(Map<String, dynamic> json) {
    return HomeCuratedObj(
      id: json['Id'],
      groupedobjectid: json['groupedobjectid'],
      noofstreams: json['noofstreams'],
      curatedobject: json['curationobj'],
    );
  }
}


class HomeCuration {
  final Map id;
  final String name;
  final String pathtothumbnail;
  final bool curator;
  final int timestamp;

  HomeCuration({
    this.id,
    this.name,
    this.pathtothumbnail,
    this.curator,
    this.timestamp,
  });

  factory HomeCuration.fromJson(Map<String, dynamic> json) {
    return HomeCuration(
      id: json['Id'],
      name: json['name'],
      pathtothumbnail: json['pathtothumbnail'],
      curator: json['curator'],
      timestamp: json['timestamp'],
    );
  }
}




class HomeUploadItemObj {
  Map id;
  String groupedobjectid;
  int noofstreams;
  List<HomeCategoryUploadItems> uploads;

  HomeUploadItemObj({
    this.id,
    this.groupedobjectid,
    this.noofstreams,
    this.uploads,
  });

  factory HomeUploadItemObj.fromJson(Map<String, dynamic> json) {
    return HomeUploadItemObj(
      id: json['Id'],
      groupedobjectid: json['groupedobjectid'],
      noofstreams: json['noofstreams'],
      uploads: json['uploads'],
    );
  }
}


class HomeCategoryUploadItems {
  final Map id;
  final String displayname;
  final String uploader;
  final bool downloadable;
  final int downloads;
  final bool private;
  final int views;
  final bool reported;
  final int timestamp;
  final String musicfilepath;
  final String thumbnailpath;
  final int musicfilesize;
  final int thumbnailsize;
  final String musicfilemimetype;
  final String thumbnailmimetype;

  HomeCategoryUploadItems({
    this.id,
    this.displayname,
    this.uploader,
    this.downloadable,
    this.downloads,
    this.private,
    this.views,
    this.reported,
    this.timestamp,
    this.musicfilepath,
    this.thumbnailpath,
    this.musicfilesize,
    this.thumbnailsize,
    this.musicfilemimetype,
    this.thumbnailmimetype,
  });

  factory HomeCategoryUploadItems.fromJson(Map<String, dynamic> json) {
    return HomeCategoryUploadItems(
      id: json['Id'],
      displayname: json['displayname'],
      uploader: json['Uploader'],
      downloadable: json['downloadable'],
      downloads: json['downloads'],
      private: json['isprivate'],
      views: json['views'],
      reported: json['reported'],
      timestamp: json['timestamp'],
      musicfilepath: json['musicfilepath'],
      thumbnailpath: json['thumbnailpath'],
      musicfilesize: json['musicfilesize'],
      thumbnailsize: json['thumbnailsize'],
      musicfilemimetype: json['musicfilemimetype'],
      thumbnailmimetype: json['thumbnailmimetype'],
    );
  }
}
