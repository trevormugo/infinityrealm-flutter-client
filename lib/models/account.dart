class Account {
  final String id;
  final String fullname;
  final String username;
  final String email;
  final String phonenumber;
  Account({
    this.id,
    this.fullname,
    this.username,
    this.email,
    this.phonenumber,
  });

  factory Account.fromJson(Map<String, dynamic> json) {
    return Account(
        id: json['id'],
        fullname: json['fullname'],
        username: json['username'],
        email: json['email'],
        phonenumber: json['phonenumber']);
  }
}
