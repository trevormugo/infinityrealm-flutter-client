import 'package:flutter/material.dart';

class CurationPageArgs {
  String curationid;
  String curationname;

  CurationPageArgs({
    @required this.curationid,
    @required this.curationname,
  });
}
