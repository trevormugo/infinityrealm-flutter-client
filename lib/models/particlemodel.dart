import 'package:flutter/material.dart';

class Particle {
  IconData iconparticles;
  Offset position;
  double random;
   
  Particle({
    this.iconparticles,
    this.position,
    this.random,
  });
}
