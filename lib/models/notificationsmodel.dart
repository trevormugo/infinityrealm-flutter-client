import 'package:flutter/material.dart';

class NotificationReq {
  String toAccount;

  String fromAccount;

  String fromAccountName;

  String fromAccountThumbnail;

  String notificationType;

  String notificationString;

  NotificationReq({
    this.toAccount,
    this.fromAccount,
    this.fromAccountName,
    this.fromAccountThumbnail,
    this.notificationType,
    this.notificationString,
  });
}
