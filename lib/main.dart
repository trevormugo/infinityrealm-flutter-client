import 'package:fancy_dialog/FancyGif.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'dart:convert';

import './restapi.dart';
import './homepage/homebackground.dart';
import './customdialog.dart';
import './forgotpassword/forgotpassword.dart';
import './signup/signup.dart';
import 'models/account.dart';
import './homepage/homeroot.dart';
import './animations/backgroundanimation.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences.getInstance().then((prefs) {
    runApp(MyApp(prefs: prefs));
  });
}

class MyApp extends StatelessWidget {
  MyApp({this.prefs});
  final prefs;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.green,
        brightness: Brightness.dark,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: _decideMainPage(),
    );
  }

  Widget _decideMainPage() {
   // if (prefs.getString('userid') == null) {
    //  return LoginPage();
   // } else {
      return HomeRoot(
       userid: "prefs.getString('userid')",
        subid: "prefs.getString('subid')",
      );
    //}
  }
}

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool _operationinprogress = false;
  @override
  void initState() {
    super.initState();
    _checkpermissions();
  }

  void _checkpermissions() async {
    if (await Permission.microphone.request().isGranted) {
      // Either the permission was already granted before or the user just granted it.
    } else {
      //open app settings to give app permissions
      showDialog(
        context: context,
        builder: (BuildContext context) {
          // return object of type Dialog
          return AlertDialog(
            title: Text("Use Microphone"),
            content: Text(
                "The visualizer engine used in this application requires you to grant it permissions to access the microphone failure to do so might result in errors or unexpected behaviour."),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              FlatButton(
                child: Text("Settings"),
                onPressed: () {
                  openAppSettings();
                },
              ),
              FlatButton(
                child: Text("Close"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    final _formKey = GlobalKey<FormState>();
    TextEditingController _emailcontroller = TextEditingController();
    TextEditingController _passcontroller = TextEditingController();

    void _createaccount() {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => SignUpPage()),
      );
    }

    void _forgotpassword() {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => ForgotPassword()),
      );
    }

    void _login() async {
      if (_formKey.currentState.validate()) {
        if (_operationinprogress == true) {
          showDialog(
            context: context,
            builder: (BuildContext context) => CustomDialog(
              title: "Warning",
              description: "Operation in progress.",
              gifpath: FancyGif.MOVE_FORWARD,
            ),
          );
        } else {
          setState(() {
            _operationinprogress = true;
          });
          var loginresponse = await RestApi().login("/useraccounts/login",
              _emailcontroller.text, _passcontroller.text);
          if (loginresponse.statusCode == 200) {
            setState(() {
              _operationinprogress = false;
            });
            var account = Account.fromJson(json.decode(loginresponse.body));
            SharedPreferences prefs = await SharedPreferences.getInstance();
            await prefs.setString('userid', account.id);
            await prefs.setString('useremail', account.email);
            await prefs.setString('userphonenumber', account.phonenumber);
            await prefs.setString('username', account.username);
            var subscriptionresponse = await RestApi().retrievesubscriptionbyrefid(
                '/accountspool/retrievesubscriptionbyrefid?refid=${account.id}&phonenumber=${account.phonenumber}&email=${account.email}');
            if (subscriptionresponse.statusCode == 200) {
              Map<String, dynamic> subscription =
                  json.decode(subscriptionresponse.body);
              await prefs.setString('subid', subscription["id"]);
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                  builder: (context) =>
                      HomeRoot(userid: account.id, subid: subscription["id"]),
                ),
              );
            } else {
              await prefs.setString('subid', null);
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                  builder: (context) =>
                      HomeRoot(userid: account.id, subid: null),
                ),
              );
            }
          } else {
            setState(() {
              _operationinprogress = false;
            });
            showDialog(
              context: context,
              builder: (BuildContext context) => CustomDialog(
                title: "Error",
                description: "Incorrect email or password.",
                gifpath: FancyGif.CHECK_MAIL,
              ),
            );
          }
        }
      }
    }

    return Scaffold(
      body: SafeArea(
        top: true,
        bottom: false,
        left: false,
        right: false,
        child: LayoutBuilder(builder: (context, constraints) {
          return SingleChildScrollView(
            child: ConstrainedBox(
              constraints: BoxConstraints(
                  minWidth: constraints.maxWidth,
                  minHeight: constraints.maxHeight),
              child: Stack(
                alignment: AlignmentDirectional.center,
                children: [
                  BackgroundAnimation(
                    nooficons: 15,
                    width: constraints.maxWidth,
                    height: constraints.maxHeight,
                    shouldstart: true,
                  ),
                  Container(
                    //height: constraints.maxHeight,
                    //width: constraints.maxWidth,
                    color: Colors.black38.withOpacity(0.4),
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      /*Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[*/
                          FlatButton(
                            onPressed: _forgotpassword,
                            child: Text("Forgot password?"),
                          ),
                          Container(
                            width: double.infinity,
                            child: TypewriterAnimatedTextKit(
                                onTap: () {
                                  print("Tap Event");
                                },
                                speed: Duration(milliseconds: 1000),
                                repeatForever: true,
                                text: [
                                  "INFINITE",
                                ],
                                textStyle: TextStyle(
                                    fontSize: 40, fontFamily: "Monoton"),
                                textAlign: TextAlign.center,
                                alignment: AlignmentDirectional.center),
                          ),
                          Form(
                            key: _formKey,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Container(
                                  width: double.infinity,
                                  height: 50,
                                  margin: EdgeInsets.all(20),
                                  child: TextFormField(
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return 'Please enter some text';
                                      }
                                      return null;
                                    },
                                    keyboardType: TextInputType.emailAddress,
                                    controller: _emailcontroller,
                                    obscureText: false,
                                    decoration: InputDecoration(
                                        border: OutlineInputBorder(
                                          borderRadius: BorderRadius.only(
                                            bottomLeft:
                                                Radius.elliptical(30, 30),
                                            topRight: Radius.elliptical(30, 30),
                                          ),
                                        ),
                                        labelText: "Enter you\'re email"),
                                  ),
                                ),
                                Container(
                                  width: double.infinity,
                                  height: 50,
                                  margin: EdgeInsets.all(20),
                                  child: TextFormField(
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return 'Please enter some text';
                                      }
                                      return null;
                                    },
                                    controller: _passcontroller,
                                    obscureText: true,
                                    keyboardType: TextInputType.visiblePassword,
                                    decoration: InputDecoration(
                                        border: OutlineInputBorder(
                                          borderRadius: BorderRadius.only(
                                            bottomLeft:
                                                Radius.elliptical(30, 30),
                                            topRight: Radius.elliptical(30, 30),
                                          ),
                                        ),
                                        labelText: "Enter you\'re password"),
                                  ),
                                ),
                                FlatButton(
                                  onPressed: _login,
                                  child: Text(
                                    "Log In",
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Center(
                            child: FractionallySizedBox(
                              widthFactor: 0.60,
                              child: Container(
                                height: 70,
                                width: 100,
                                child: (_operationinprogress == true)
                                    ? CircularProgressIndicator(
                                        backgroundColor: Colors.green)
                                    : Container(),
                              ),
                            ),
                          ),
                          FlatButton(
                            onPressed: _createaccount,
                            child: Text(
                              "Dont have an account?",
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                //],
              //),
            ),
          );
        }),
      ),
    );
  }
}
