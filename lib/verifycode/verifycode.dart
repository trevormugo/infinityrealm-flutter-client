import 'dart:convert';

import 'package:fancy_dialog/FancyGif.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../models/account.dart';

import '../accountpool/accountpool.dart';
import '../appbar.dart';
import '../changepassword/changepassword.dart';
import '../customdialog.dart';
import '../restapi.dart';
import '../animations/backgroundanimation.dart';

class VerifyCode extends StatefulWidget {
  VerifyCode(
      {@required this.accountcreation,
      @required this.emailrecipient,
      this.signupbody});
  final bool accountcreation;
  final String emailrecipient;
  final Map signupbody;
  @override
  _VerifyCodeInstance createState() => _VerifyCodeInstance();
}

class _VerifyCodeInstance extends State<VerifyCode> {
  TextEditingController _codecontroller = TextEditingController();
  int counter = 0;
  void _sendcode() async {
    if (counter == 4) {
      if (widget.accountcreation == true) {
        var deletedresponse = await RestApi().deleteverificationresource(
            '/verification/deleteresource?email=${widget.emailrecipient}');
        if (deletedresponse.statusCode == 204) {
          showDialog(
            context: context,
            builder: (BuildContext context) => CustomDialog(
                description: "No more attempts",
                gifpath: FancyGif.CHECK_MAIL,
                title: "Bad request",
                onok: () {
                  Navigator.pop(context);
                }),
          );
        } else {
          showDialog(
            context: context,
            builder: (BuildContext context) => CustomDialog(
                description: "Server error",
                gifpath: FancyGif.CHECK_MAIL,
                title: "Bad request",
                onok: () {
                  Navigator.pop(context);
                }),
          );
        }
      } else {
        showDialog(
          context: context,
          builder: (BuildContext context) => CustomDialog(
              description: "No more attempts",
              gifpath: FancyGif.CHECK_MAIL,
              title: "Bad request",
              onok: () {
                Navigator.pop(context);
              }),
        );
      }
    } else {
      if (_codecontroller.text == null) {
        showDialog(
          context: context,
          builder: (BuildContext context) => CustomDialog(
              description: "Please Input Something",
              gifpath: FancyGif.CHECK_MAIL,
              title: "Bad request"),
        );
      } else {
        var response = await RestApi().verifycode(
            "/verification/verifybymodel?email=${widget.emailrecipient}&code=${_codecontroller.text}");
        if (response.statusCode == 200) {
          showDialog(
            context: context,
            builder: (BuildContext context) => CustomDialog(
                description: "success",
                gifpath: FancyGif.CHECK_MAIL,
                title: "Success"),
          );
          if (widget.accountcreation == false) {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (context) => PasswordChange(
                  email: widget.emailrecipient,
                ),
              ),
            );
          } else {
            var signupresponse = await RestApi()
                .signup("/useraccounts/signup", widget.signupbody);
            if (signupresponse.statusCode == 201) {
              //successful signup move to pricing
              var account = Account.fromJson(json.decode(signupresponse.body));
              SharedPreferences prefs = await SharedPreferences.getInstance();
              await prefs.setString('userid', account.id);
              await prefs.setString('useremail', account.email);
              await prefs.setString('userphonenumber', account.phonenumber);
              await prefs.setString('username', account.username);
              print(account.username);
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) => AccountPool(
                          id: account.id,
                          email: account.email,
                          phonenumber: account.phonenumber,
                          username: account.username,
                        )),
              );
            } else {
              showDialog(
                context: context,
                builder: (BuildContext context) => CustomDialog(
                    description: "Email already exists",
                    gifpath: FancyGif.CHECK_MAIL,
                    title: "Bad request"),
              );
            }
          }
        } else {
          counter++;
          showDialog(
            context: context,
            builder: (BuildContext context) => CustomDialog(
                description: "Incorrect code.Attemps (${4 - counter})",
                gifpath: FancyGif.CHECK_MAIL,
                title: "Bad request"),
          );
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBarOnly(
          titletext: "Verify code",
        ),
      ),
      extendBodyBehindAppBar: true,
      body: SafeArea(
        top: true,
        bottom: false,
        left: false,
        right: false,
        child: LayoutBuilder(
          builder: (context, constraints) {
            return SingleChildScrollView(
              child: ConstrainedBox(
                constraints: BoxConstraints(
                    minWidth: constraints.maxWidth,
                    minHeight: constraints.maxHeight),
                child: Stack(
                  alignment: AlignmentDirectional.center,
                  children: [
                    BackgroundAnimation(
                      nooficons: 15,
                      width: constraints.maxWidth,
                      height: constraints.maxHeight,
                      shouldstart: true,
                    ),
                    Container(
                      height: constraints.maxHeight,
                      width: constraints.maxWidth,
                      color: Colors.black38.withOpacity(0.4),
                    ),
                    Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          width: double.infinity,
                          height: 50,
                          margin: EdgeInsets.all(20),
                          child: TextFormField(
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Please enter some text';
                              }
                              return null;
                            },
                            controller: _codecontroller,
                            obscureText: false,
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.elliptical(30, 30),
                                    topRight: Radius.elliptical(30, 30),
                                  ),
                                ),
                                labelText: "Enter you\'re code"),
                          ),
                        ),
                        FlatButton(
                          onPressed: _sendcode,
                          child: Text(
                            "Verify Code",
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
