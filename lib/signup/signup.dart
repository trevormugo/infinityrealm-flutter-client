import 'package:flutter/material.dart';
import 'package:fancy_dialog/FancyGif.dart';

import '../appbar.dart';
import '../verifycode/verifycode.dart';
import '../restapi.dart';
import '../customdialog.dart';
import '../animations/backgroundanimation.dart';

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageInstance createState() => _SignUpPageInstance();
}

class _SignUpPageInstance extends State<SignUpPage> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _fullnamecontroller = TextEditingController();
  TextEditingController _usernamecontroller = TextEditingController();
  TextEditingController _phonenumbercontroller = TextEditingController();
  TextEditingController _emailcontroller = TextEditingController();
  TextEditingController _passcontroller = TextEditingController();
  TextEditingController _repeatpasswordcontroller = TextEditingController();
  static Map<String, String> values;
  void _signup() async {
    if (_formKey.currentState.validate()) {
      if (_passcontroller.text != _repeatpasswordcontroller.text) {
        showDialog(
          context: context,
          builder: (BuildContext context) => CustomDialog(
              description: "Passwords dont match",
              gifpath: FancyGif.FUNNY_MAN,
              title: "Not matching"),
        );
      } else {
        values = {
          "FullName": _fullnamecontroller.text,
          "UserName": _usernamecontroller.text,
          "PhoneNUmber": _phonenumbercontroller.text.trim(),
          "Email": _emailcontroller.text.trim(),
          "Password": _passcontroller.text,
        };
        var response = await RestApi().sendemailtocreateaccount(
            "/verification/signup", _emailcontroller.text.trim());
        if (response.statusCode == 201) {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => VerifyCode(
                      accountcreation: true,
                      emailrecipient: _emailcontroller.text.trim(),
                      signupbody: values,
                    )),
          );
        } else if (response.statusCode == 400) {
          showDialog(
            context: context,
            builder: (BuildContext context) => CustomDialog(
                description: "This account was already verified",
                gifpath: FancyGif.FUNNY_MAN,
                title: "Already verified"),
          );
        } else {
          showDialog(
            context: context,
            builder: (BuildContext context) => CustomDialog(
                description: "Code not sent",
                gifpath: FancyGif.FUNNY_MAN,
                title: "Server error"),
          );
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBarOnly(
          titletext: "Sign up",
        ),
      ),
      extendBodyBehindAppBar: true,
      body: SafeArea(
        top: true,
        bottom: false,
        left: false,
        right: false,
        child: LayoutBuilder(builder: (context, constraints) {
          return SingleChildScrollView(
            child: ConstrainedBox(
              constraints: BoxConstraints(
                  minWidth: constraints.maxWidth,
                  minHeight: constraints.maxHeight),
              child: Stack(
                alignment: AlignmentDirectional.center,
                children: [
                  BackgroundAnimation(
                    nooficons: 15,
                    width: constraints.maxWidth,
                    height: constraints.maxHeight,
                    shouldstart: true,
                  ),
                  Container(
                    height: constraints.maxHeight,
                    width: constraints.maxWidth,
                    color: Colors.black38.withOpacity(0.4),
                  ),
                  Form(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Container(
                          width: double.infinity,
                          height: 50,
                          margin: EdgeInsets.all(20),
                          child: TextFormField(
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Please enter some text';
                              }
                              return null;
                            },
                            controller: _fullnamecontroller,
                            obscureText: false,
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.elliptical(30, 30),
                                    topRight: Radius.elliptical(30, 30),
                                  ),
                                ),
                                labelText: "Enter you\'re Full name"),
                          ),
                        ),
                        Container(
                          width: double.infinity,
                          height: 50,
                          margin: EdgeInsets.all(20),
                          child: TextFormField(
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Please enter some text';
                              }
                              return null;
                            },
                            controller: _usernamecontroller,
                            obscureText: false,
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.elliptical(30, 30),
                                    topRight: Radius.elliptical(30, 30),
                                  ),
                                ),
                                labelText: "Enter you\'re user name"),
                          ),
                        ),
                        Container(
                          width: double.infinity,
                          height: 50,
                          margin: EdgeInsets.all(20),
                          child: TextFormField(
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Please enter some text';
                              }
                              return null;
                            },
                            controller: _phonenumbercontroller,
                            obscureText: false,
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.elliptical(30, 30),
                                    topRight: Radius.elliptical(30, 30),
                                  ),
                                ),
                                labelText:
                                    "Enter you\'re phone number(+254 format)"),
                          ),
                        ),
                        Container(
                          width: double.infinity,
                          height: 50,
                          margin: EdgeInsets.all(20),
                          child: TextFormField(
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Please enter some text';
                              }
                              return null;
                            },
                            controller: _emailcontroller,
                            obscureText: false,
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.elliptical(30, 30),
                                    topRight: Radius.elliptical(30, 30),
                                  ),
                                ),
                                labelText: "Enter you\'re email"),
                          ),
                        ),
                        Container(
                          width: double.infinity,
                          height: 50,
                          margin: EdgeInsets.all(20),
                          child: TextFormField(
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Please enter some text';
                              }
                              return null;
                            },
                            controller: _passcontroller,
                            obscureText: true,
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.elliptical(30, 30),
                                    topRight: Radius.elliptical(30, 30),
                                  ),
                                ),
                                labelText: "Enter you\'re password"),
                          ),
                        ),
                        Container(
                          width: double.infinity,
                          height: 50,
                          margin: EdgeInsets.all(20),
                          child: TextFormField(
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Please enter some text';
                              }
                              return null;
                            },
                            controller: _repeatpasswordcontroller,
                            obscureText: true,
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.elliptical(30, 30),
                                    topRight: Radius.elliptical(30, 30),
                                  ),
                                ),
                                labelText: "Repeat password"),
                          ),
                        ),
                        FlatButton(
                          onPressed: _signup,
                          child: Text(
                            "Create Account",
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        }),
      ),
    );
  }
}
