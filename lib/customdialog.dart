import 'package:flutter/material.dart';
import 'package:fancy_dialog/fancy_dialog.dart';

class CustomDialog extends StatefulWidget {
  CustomDialog(
      {@required this.description,
      @required this.gifpath,
      @required this.title,
      this.onok,
      this.oncancel});
  final String description;
  final String gifpath;
  final String title;
  final Function onok;
  final Function oncancel;
  @override
  _CustomDialogInstance createState() => _CustomDialogInstance();
}

class _CustomDialogInstance extends State<CustomDialog> {
  @override
  Widget build(BuildContext context) {
    return FancyDialog(
      title: widget.title,
      descreption: widget.description,
      gifPath: widget.gifpath,
      okFun: widget.onok,
    );
  }
}
