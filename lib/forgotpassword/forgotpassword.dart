import 'package:fancy_dialog/FancyGif.dart';
import 'package:flutter/material.dart';

import '../appbar.dart';
import '../customdialog.dart';
import '../verifycode/verifycode.dart';

import '../restapi.dart';
import '../animations/backgroundanimation.dart';

class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordInstance createState() => _ForgotPasswordInstance();
}

class _ForgotPasswordInstance extends State<ForgotPassword> {
  TextEditingController _emailcontroller = TextEditingController();
  void _sendemail() async {
    if (_emailcontroller.text == null) {
      showDialog(
        context: context,
        builder: (BuildContext context) => CustomDialog(
            description: "Please input something",
            gifpath: FancyGif.FUNNY_MAN,
            title: "Input required"),
      );
    } else {
      var response = await RestApi().sendemailtochangepasswd(
          "/verification/resetpassword", _emailcontroller.text.trim());
      if (response.statusCode == 200) {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => VerifyCode(
                    accountcreation: false,
                    emailrecipient: _emailcontroller.text.trim(),
                  )),
        );
      } else if (response.statusCode == 400) {
        showDialog(
          context: context,
          builder: (BuildContext context) => CustomDialog(
              description: "Account must have not been verified during sign up",
              gifpath: FancyGif.FUNNY_MAN,
              title: "Bad Request"),
        );
      } else {
        showDialog(
          context: context,
          builder: (BuildContext context) => CustomDialog(
              description: "Code not sent",
              gifpath: FancyGif.FUNNY_MAN,
              title: "Server error"),
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBarOnly(
          titletext: "Password help",
        ),
      ),
      extendBodyBehindAppBar: true,
      body: SafeArea(
        top: true,
        bottom: false,
        left: false,
        right: false,
        child: LayoutBuilder(
          builder: (context, constraints) {
            return SingleChildScrollView(
              child: ConstrainedBox(
                constraints: BoxConstraints(
                    minWidth: constraints.maxWidth,
                    minHeight: constraints.maxHeight),
                child: Stack(
                  alignment: AlignmentDirectional.center,
                  children: [
                    BackgroundAnimation(
                      nooficons: 15,
                      width: constraints.maxWidth,
                      height: constraints.maxHeight,
                      shouldstart: true,
                    ),
                    Container(
                      height: constraints.maxHeight,
                      width: constraints.maxWidth,
                      color: Colors.black38.withOpacity(0.4),
                    ),
                    Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          width: double.infinity,
                          height: 50,
                          margin: EdgeInsets.all(20),
                          child: TextFormField(
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Please enter some text';
                              }
                              return null;
                            },
                            controller: _emailcontroller,
                            obscureText: false,
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.elliptical(30, 30),
                                    topRight: Radius.elliptical(30, 30),
                                  ),
                                ),
                                labelText: "Enter you\'re email"),
                          ),
                        ),
                        FlatButton(
                          onPressed: _sendemail,
                          child: Text(
                            "Send code",
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
