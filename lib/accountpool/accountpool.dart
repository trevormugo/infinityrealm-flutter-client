import 'package:flutter/material.dart';
import 'package:shape_of_view/shape_of_view.dart';
import '../appbar.dart';
import '../homepage/homebackground.dart';
import './customerdetails.dart';
import '../animations/backgroundanimation.dart';
import '../homepage/homeroot.dart';

class AccountPool extends StatefulWidget {
  AccountPool(
      {@required this.id,
      @required this.email,
      @required this.phonenumber,
      @required this.username});
  final String id;
  final String email;
  final String phonenumber;
  final String username;
  @override
  _AccountPoolInstance createState() => _AccountPoolInstance();
}

class _AccountPoolInstance extends State<AccountPool> {
  List<String> premiumperks = [
    //ads will be google skippable video ads ,
    //google native ads in list view and columns category
    //and custom ads as well
    //custom ads will be first priority ..and will be as a result from a request from a client
    //to post an ad request one will have to specify a region in which we eill show the ads ...if its more than one ,
    //one will have to pay an extra fee as well as the duration of the ad in that region max 4wks the fee increases as
    //you increase the weeks....if there are no custom ads for a particular region we will be resorting to google ads in that particular part where
    //a custom ad was suppose to be
    "No ads",
    //trending playlists to be added in fanfavorite
    "Fan Favourite",
    //there will be a time limit for downloaded content due to copywrites
    //we will keep the file in the applications directory and delete it after 30 days
    //but we will save a history of downloaded files not all files will be downloadable and thw views will still be counted once you
    //back online, the downloads will also be counted on the database so that either the owner or us will be able to see
    //how popular it is , if it is very popular we will know and will use that data to suggest
    //downloads likes and views are the reactions we will be collecting as data to suggest content and / or accounts

    "Download media for offline use",
    //If you want exclusive beats you will have to buy them , the rest of them can be used and reused by others

    "Beats leasing - we lease you beats to help you in youre creations",
    //Send a request for us to upload youre content on the platform
    //we will review the content and either approve or disapprove it for upload
    //you will earn royalties from the views of youre content
    //still debating whether to remove the upload if it doesnt get enough attention

    "Upload requests",
    "Earn from your uploads",

    //A badge that shows you're account is a premium account
    "Premium badge",

    //we can achieve this on the search page we make sure that youre account is at least on the
    //list of suggested accounts at least once or twice every week , increase this chance by having many followers
    "Increased Account Discovery not more than verified artists though",

    //request for us to upload an event for you , youre request will be reviewed and if approved we will post youre event
    //to increase the chances of approval have many followers and reactions on youre content
    //requested event should happen at least three weeks before ...if not a fee will need to be paid and if paid
    //youre request will be automatically approved
    //a notification will be posted to youre followers for every event posted
    "Event upload requests",

    //during live podcasts we will be selectively choosing the most popular uploads among the premium users and playing it
    //for the listeners to hear
    //for this we will have to use the app player itself so if you were listening to something before coming to the podcast,
    //it will be stopped and i will have to research a way to have a link posted to that upload for users to add to their curated playlists
    //the player automatically has a route to view the account
    "Get a chance to advertise youre content on live podcasts",

    //most popular accounts will have a chance to get interviewed during a podcast
    "Get a chance to be interviewed in live podcasts , get shoutouts , general account advertising"
  ];

  void _subscribe() {
    //add a customer then add a card on file then subcribe to a plan
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => CustomerDetails(
          email: widget.email,
          phonenumber: widget.phonenumber,
          id: widget.id,
          username: widget.username,
        ),
      ),
    );
  }

  void _freeaccount() {
    //do nothing and continue with account as it is
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => HomeRoot(
          userid: widget.id,
          subid: null,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBarOnly(
          titletext: "Premium Account",
        ),
      ),
      extendBodyBehindAppBar: true,
      body: SafeArea(
        top: true,
        bottom: false,
        left: false,
        right: false,
        child: LayoutBuilder(builder: (context, constraints) {
          return SingleChildScrollView(
            child: ConstrainedBox(
              constraints: BoxConstraints(
                  minWidth: constraints.maxWidth,
                  minHeight: constraints.maxHeight),
              child: Stack(
                alignment: AlignmentDirectional.center,
                children: [
                  BackgroundAnimation(
                    nooficons: 15,
                    width: constraints.maxWidth,
                    height: constraints.maxHeight,
                    shouldstart: true,
                  ),
                  Container(
                    width: constraints.maxWidth,
                    height: constraints.maxHeight,
                    color: Colors.black38.withOpacity(0.4),
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      FlatButton(
                        onPressed: _freeaccount,
                        child: Text("Continue with free Account"),
                      ),
                      ShapeOfView(
                        elevation: 4,
                        height: 500,
                        width: double.infinity,
                        shape: DiagonalShape(
                            position: DiagonalPosition.Bottom,
                            direction: DiagonalDirection.Right,
                            angle: DiagonalAngle.deg(angle: 10)),
                        child: Center(
                          child: Text("200ksh"),
                        ),
                      ),
                      ListView.builder(
                        itemCount: premiumperks.length,
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemBuilder: (context, index) {
                          return ListTile(
                            leading: Icon(Icons.format_list_bulleted),
                            title: Text(premiumperks[index]),
                          );
                        },
                      ),
                      FlatButton(
                        onPressed: _subscribe,
                        child: Text("Subcribe to premium Account"),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          );
        }),
      ),
    );
  }
}
