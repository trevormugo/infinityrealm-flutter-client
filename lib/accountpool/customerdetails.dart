import 'dart:convert';

import 'package:fancy_dialog/FancyGif.dart';
import 'package:flutter/material.dart';
import 'package:infinityrealm/homepage/homebackground.dart';
import 'package:square_in_app_payments/in_app_payments.dart';
import 'package:square_in_app_payments/models.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geocoder/geocoder.dart';

import '../appbar.dart';
import '../customdialog.dart';
import '../restapi.dart';
import '../animations/backgroundanimation.dart';
import '../homepage/homeroot.dart';

class CustomerDetails extends StatefulWidget {
  CustomerDetails({
    @required this.email,
    @required this.phonenumber,
    @required this.id,
    @required this.username,
  });
  final String email;
  final String phonenumber;
  final String id;
  final String username;
  @override
  _CustomerDetailsInstance createState() => _CustomerDetailsInstance();
}

class _CustomerDetailsInstance extends State<CustomerDetails> {
  @override
  void initState() {
    super.initState();
    InAppPayments.setSquareApplicationId(
        'sandbox-sq0idb-2sJMKHOOa92UD4-_0uVzxA');
    initializepermissions();
    _locationsdemostate();
  }

  final _formKey = GlobalKey<FormState>();
  TextEditingController _firstnamescontroller = TextEditingController();
  TextEditingController _surnamecontroller = TextEditingController();
  TextEditingController _phonecontroller = TextEditingController();
  Map<String, dynamic> customervalues;
  Map<String, dynamic> cardvalues;
  Map<String, dynamic> subcriptionvalues;
  String locationid;
  Contact contact;

  void _locationsdemostate() {
    locationid = "LD0XHDP99B4X0";
  }

  void initializepermissions() async {
    LocationPermission checkpermission = await checkPermission();
    if (checkpermission == LocationPermission.whileInUse ||
        checkpermission == LocationPermission.always) {
      print("permissions granted");
    } else {
      LocationPermission requestpermission = await requestPermission();
    }
  }

  Future<List<Address>> _getlocations() async {
    Position position =
        await getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    final coordinates = new Coordinates(position.latitude, position.longitude);
    var addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    print(addresses.first.countryName);
    return addresses;
  }

  void _create() async {
    if (_formKey.currentState.validate()) {
      List<Address> locations = await _getlocations();
      contact = Contact((b) => b
        ..givenName = _firstnamescontroller.text
        ..familyName = _surnamecontroller.text
        ..city = locations.first.locality
        ..countryCode = locations.first.countryCode
        ..email = widget.email
        ..phone = _phonecontroller.text);
      customervalues = {
        "Id": widget.id,
        "Firstnames": _firstnamescontroller.text.trim(),
        "Surname": _surnamecontroller.text.trim(),
        "Username": widget.username.trim(),
        "Email": widget.email.trim(),
        "Phonenumber": _phonecontroller.text.trim(),
      };
      var customerresponse = await RestApi().createcustomerrequest(
          "/accountspool/createcustomer", customervalues);
      Map<String, dynamic> internalcustomer =
          json.decode(customerresponse.body);
      if (customerresponse.statusCode == 201) {
        /***** CUSTOMER CREATED *****/
        var money = Money((b) => b
          ..amount = 2
          ..currencyCode = 'USD');
        await InAppPayments.startCardEntryFlowWithBuyerVerification(
          squareLocationId: locationid,
          contact: contact,
          buyerAction: "STORE",
          collectPostalCode: true,
          money: money,
          onBuyerVerificationSuccess: (BuyerVerificationDetails details) async {
            print(details);
            var subscriptionresponse = await RestApi().getsubcriptionplanbycountry(
                "/accountspool/searchspecificsubscriptionplan/${locations.first.countryName}");
            Map<String, dynamic> subres =
                json.decode(subscriptionresponse.body);
            if (subscriptionresponse.statusCode == 200) {
              /***** SUBSCRIPTION PLAN FOUND *****/
              cardvalues = {
                "Cardnonce": details.nonce,
                "CustomerId": internalcustomer['id'],
                "FullName":
                    _firstnamescontroller.text + " " + _surnamecontroller.text,
                "VerificationToken": details.token,
              };
              var cardresponse = await RestApi().createcustomercardrequest(
                  "/accountspool/createcustomercard", cardvalues);
              Map<String, dynamic> card = json.decode(cardresponse.body);
              if (cardresponse.statusCode == 200) {
                /***** CUSTOMER CARD CREATED *****/
                subcriptionvalues = {
                  "LocationId": "LD0XHDP99B4X0",
                  "PlanId": subres['id'],
                  "CustomerId": internalcustomer['id'],
                  "CardId": card['id'],
                  "Refid": widget.id,
                  "Phonenumber": _phonecontroller.text,
                  "Email": widget.email,
                };
                var createsubscription = await RestApi().createsubscription(
                    "/accountspool/createsubscription", subcriptionvalues);
                if (createsubscription.statusCode == 201) {
                  /*****  SUBSCRIBED  *****/
                  Map<String, dynamic> subscription =
                      json.decode(createsubscription.body);
                  print(createsubscription);
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) => HomeRoot(
                        userid: widget.id,
                        subid: subscription['id'],
                      ),
                    ),
                  );
                } else if (createsubscription.statusCode == 204) {
                  errorhandler("No Cards Found");
                } else {
                  errorhandler("Subscription");
                }
              } else {
                errorhandler("Card creation");
              }
            } else {
              errorhandler("Get Plans");
            }
          },
          onBuyerVerificationFailure: (ErrorInfo error) {
            InAppPayments.showCardNonceProcessingError(
                "Verification or processing error");
          },
          onCardEntryCancel: () {
            print("canceled");
          },
        );
      } else {
        errorhandler("Create Customer");
      }
    }
  }

  void errorhandler(String errortitle) {
    showDialog(
      context: context,
      builder: (BuildContext context) => CustomDialog(
        description: "There is a problem with the request body",
        gifpath: FancyGif.FUNNY_MAN,
        title: errortitle,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBarOnly(
          titletext: "Customer Details",
        ),
      ),
      extendBodyBehindAppBar: true,
      body: SafeArea(
        top: true,
        bottom: false,
        left: false,
        right: false,
        child: LayoutBuilder(builder: (context, constraints) {
          return SingleChildScrollView(
            child: ConstrainedBox(
              constraints: BoxConstraints(
                  minWidth: constraints.maxWidth,
                  minHeight: constraints.maxHeight),
              child: Stack(
                alignment: AlignmentDirectional.center,
                children: [
                  BackgroundAnimation(
                    nooficons: 15,
                    width: constraints.maxWidth,
                    height: constraints.maxHeight,
                    shouldstart: true,
                  ),
                  Container(
                    width: constraints.maxWidth,
                    height: constraints.maxHeight,
                    color: Colors.black38.withOpacity(0.4),
                  ),
                  Form(
                    key: _formKey,
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          width: double.infinity,
                          height: 50,
                          margin: EdgeInsets.all(20),
                          child: TextFormField(
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Please enter some text';
                              }
                              return null;
                            },
                            controller: _firstnamescontroller,
                            obscureText: false,
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.elliptical(30, 30),
                                    topRight: Radius.elliptical(30, 30),
                                  ),
                                ),
                                labelText: "Enter you\'re Given names"),
                          ),
                        ),
                        Container(
                          width: double.infinity,
                          height: 50,
                          margin: EdgeInsets.all(20),
                          child: TextFormField(
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Please enter some text';
                              }
                              return null;
                            },
                            controller: _surnamecontroller,
                            obscureText: false,
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.elliptical(30, 30),
                                    topRight: Radius.elliptical(30, 30),
                                  ),
                                ),
                                labelText: "Enter you\'re Surname"),
                          ),
                        ),
                        Container(
                          width: double.infinity,
                          height: 50,
                          margin: EdgeInsets.all(20),
                          child: TextFormField(
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Please enter some text';
                              }
                              return null;
                            },
                            controller: _phonecontroller,
                            obscureText: false,
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.elliptical(30, 30),
                                    topRight: Radius.elliptical(30, 30),
                                  ),
                                ),
                                labelText:
                                    "Enter you\'re Phonenumber(country code format)"),
                          ),
                        ),
                        FlatButton(
                          onPressed: _create,
                          child: Text(
                            "Create",
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        }),
      ),
    );
  }
}
