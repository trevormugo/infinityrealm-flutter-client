import "./audio/actuallaudioplayer.dart";
import "./video/actuallvideotwo.dart";

//use this class to manage the players and the transitions between mini players and fullscreen players
class PlayerManager {
  void pause(String playertype) {
    if (playertype == "video") {
      VideoAppState.playerController.pause();
    } else {
      AudioInstance.audioPlayer.pause();
    }
  }

  void play(String playertype) {
    if (playertype == "video") {
      VideoAppState.playerController.play();
    } else {
      AudioInstance.audioPlayer.resume();
    }
  }

 





  void seek(String playertype, int time) {}
}
