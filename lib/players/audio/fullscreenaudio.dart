import 'dart:async';
import 'dart:ui';
import 'package:line_icons/line_icons.dart';
import 'package:flutter/material.dart';
import '../../animations/iconanimation.dart';
import './actuallaudioplayer.dart';
import '../playermanager.dart';
import '../retrievetext.dart';

class FullScreenAudioPlayer extends StatefulWidget {
  FullScreenAudioPlayer({
    @required this.songname,
    @required this.id,
    @required this.screenwidth,
    @required this.screenheight,
  });
  final String songname;
  final String id;
  final double screenwidth;
  final double screenheight;
  @override
  State<StatefulWidget> createState() {
    return FullAudioPlayer();
  }
}

class FullAudioPlayer extends State<FullScreenAudioPlayer> {
  static double value = 0;
  bool _visible;
  static StreamController<double> streamcontroler =
      StreamController.broadcast();
  static StreamSubscription<double> streamsubscription;
  void _toggle() {
    if (AudioInstance.isPlaying) {
      //youre palying show pause icon
      _visible = false;
      AudioInstance.pause();
      setState(() {});
    } else {
      //youre pausing show play icon
      _visible = true;
      AudioInstance.resume();
      setState(() {});
    }
  }

  @override
  void initState() {
    super.initState();
    //_value = AudioInstance.progress;
    _visible = AudioInstance.isPlaying;
    Stream mystream = streamcontroler.stream;
    streamsubscription = mystream.listen((event) {
      setState(() {
        value = event * 100;
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
    streamsubscription?.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: AlignmentDirectional.center,
      fit: StackFit.expand,
      children: <Widget>[
        SafeArea(
          top: true,
          bottom: true,
          child: Align(
            alignment: Alignment.topLeft,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(5),
                  margin: EdgeInsets.only(
                    bottom: 10,
                  ),
                  color: Colors.black.withOpacity(0.5),
                  child: Text(
                    "Music Name",
                    softWrap: true,
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(5),
                  color: Colors.black.withOpacity(0.5),
                  child: Text(
                    "Artist Name",
                    softWrap: true,
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                ),
              ],
            ),
          ),
        ),
        Container(
          // height: double.infinity,
          // width: double.infinity,
          decoration: BoxDecoration(
            color: Colors.white,
            gradient: LinearGradient(
              begin: FractionalOffset.topCenter,
              end: FractionalOffset.bottomCenter,
              colors: [
                Colors.transparent,
                Colors.black.withOpacity(0.9),
              ],
              stops: [0.0, 1.0],
            ),
          ),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                child: Slider(
                    value: value,
                    min: 0,
                    max: 100,
                    inactiveColor: Colors.black26,
                    activeColor: Colors.orange[400],
                    onChanged: (double value) {
                      setState(() {
                        value = value;
                      });
                    }),
              ),
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      IconAnimation(
                          color: Colors.green,
                          icon: LineIcons.headphones,
                          shouldanimate: false),
                      RetrieveText(
                        texttoshow: "views",
                        uploadid: widget.id,
                      ),
                    ],
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      IconAnimation(
                          color: Colors.red,
                          icon: LineIcons.heart,
                          shouldanimate: true),
                      RetrieveText(
                        texttoshow: "views",
                        uploadid: widget.id,
                      ),
                    ],
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      IconAnimation(
                          color: Colors.orange,
                          icon: LineIcons.comments,
                          shouldanimate: true),
                      RetrieveText(
                        texttoshow: "views",
                        uploadid: widget.id,
                      ),
                    ],
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      IconAnimation(
                          color: Colors.white,
                          icon: LineIcons.share,
                          shouldanimate: true),
                      RetrieveText(
                        texttoshow: "views",
                        uploadid: widget.id,
                      ),
                    ],
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Icon(LineIcons.bars, color: Colors.white, size: 25),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
        GestureDetector(
          onTap: _toggle,
          child: AnimatedContainer(
            duration: Duration(milliseconds: 500),
            height:
                _visible ? widget.screenheight * (3 / 10) : widget.screenheight,
            width: widget.screenwidth,
            //color: Colors.black.withOpacity(0.8),
            child: AnimatedOpacity(
              opacity: _visible ? 0.0 : 1.0,
              duration: Duration(milliseconds: 500),
              child: BackdropFilter(
                filter: ImageFilter.blur(
                  sigmaX: 9.0,
                  sigmaY: 9.0,
                ),
                child: Center(
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        LineIcons.angle_left,
                        color: Colors.white,
                        size: 40.0,
                      ),
                      Icon(
                        LineIcons.play,
                        color: Colors.white,
                        size: 40.0,
                      ),
                      Icon(
                        LineIcons.angle_right,
                        color: Colors.white,
                        size: 40.0,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
