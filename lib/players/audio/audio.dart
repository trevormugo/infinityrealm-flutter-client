import 'package:flutter/material.dart';
import 'package:flutter_exoplayer/audioplayer.dart';
import './fullscreenaudio.dart';
import '../../ip.dart';
import './actuallaudioplayer.dart';
import '../playermanager.dart';

class AudioPlayerFoot extends StatefulWidget {
  AudioPlayerFoot({
    @required this.audioname,
  });
  final String audioname;
  //use uploader 2 coz its a string value
  State<StatefulWidget> createState() {
    return AudioPlayerInstance();
  }
}

class AudioPlayerInstance extends State<AudioPlayerFoot>
    with TickerProviderStateMixin {
  AnimatedIconData icon = AnimatedIcons.pause_play;
  AnimationController _animatedicon;
  void _animatepauseplay() {
    if (AudioInstance.isPlaying) {
      _animatedicon.forward();
      AudioInstance.pause();
    } else {
      //youre pausing show play icon
      _animatedicon.reverse();
      AudioInstance.resume();
    }
  }

  @override
  void initState() {
    super.initState();
    _animatedicon = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 500),
    );
  }

  @override
  void dispose() async {
    _animatedicon?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Text(
          widget.audioname,
          style: TextStyle(
              fontSize: 14, color: Colors.white, fontFamily: 'NanumGothic'),
          textAlign: TextAlign.center,
        ),
        GestureDetector(
          onTap: _animatepauseplay,
          child: AnimatedIcon(
            icon: icon,
            progress: _animatedicon,
            color: Colors.white,
          ),
        ),
      ],
    );
  }
}
