import "package:flutter/material.dart";
import 'package:flutter_exoplayer/audioplayer.dart';
import 'package:flutter_exoplayer/audio_notification.dart';
import '../../ip.dart';
import './visualiser.dart';
import 'fullscreenaudio.dart';

class ActuallAudio extends StatefulWidget {
  ActuallAudio({@required this.srcid});
  final String srcid;
  @override
  State<StatefulWidget> createState() {
    return AudioInstance();
  }
}

class AudioInstance extends State<ActuallAudio> {
  static AudioPlayer audioPlayer;
  static bool isPlaying = true;
  static bool isStarted = false;
  static Duration duration;
  static Duration time;
  static String timeLeft = "";
  static double progress = 0.0;
  /*static AudioNotification audioObject = AudioNotification(
    smallIconFileName: "ic_launcher.png",
    title: "title",
    subTitle: "artist",
    largeIconUrl: "ic_launcher.png",
    isLocal: false,
    notificationDefaultActions: NotificationDefaultActions.ALL,
    notificationCustomActions: NotificationCustomActions.TWO,
  );*/
  int id = 0;
  static void startPlaying(String t) async {
    if (!isStarted) {
      await audioPlayer.play(
        Adress.myip + "/uploads/streamuploadfile/" + t,
        repeatMode: true,
        respectAudioFocus: true,
        playerMode: PlayerMode.BACKGROUND,
      );
    } else {
      await audioPlayer.resume();
    }
  }

  static void updateplayer(String route) {
    audioPlayer.dispose();
    isStarted = false;
    isPlaying = false;
    startPlaying(route);
  }

  void getTimeLeft() {
    if (duration == null) {
      setState(() {
        timeLeft = "Time Left 0s";
      });
    } else {
      setState(() {
        timeLeft = "Time Left ${duration.inSeconds}s";
      });
    }
  }

  void getProgress() {
    if (time == null || duration == null) {
      setState(() {
        progress = 0.0;
      });
    } else {
      setState(() {
        progress = time.inMilliseconds / (duration.inMilliseconds);
      });
    }
  }

  static void pause() async {
    await audioPlayer.pause();
  }

  static void resume() async {
    await audioPlayer.resume();
  }

  @override
  void initState() {
    super.initState();
    audioPlayer = AudioPlayer();
    progress = 0.0;
    audioPlayer.onAudioPositionChanged.listen((Duration p) async {
      // print('Current position: $p');
      time = await audioPlayer.getDuration();
      duration = p;
      if (duration == null) {
        timeLeft = "Time Left 0s/0s";
      } else {
        timeLeft =
            "Time Left ${duration.inSeconds}s/${time.inMilliseconds / 1000}s";
      }
      if (time == null || duration == null) {
        progress = 0.0;
      } else {
        progress = (duration.inMilliseconds) / time.inMilliseconds;
      }
      FullAudioPlayer.streamcontroler.add(progress);
      setState(() {});
    });
    audioPlayer.onAudioSessionIdChange.listen((audioSessionId) {
      id = audioSessionId;
      setState(() {});
    });
    //listen to whether the audio player is playing or paused
    audioPlayer.onPlayerStateChanged.listen((PlayerState state) {
      if (state == PlayerState.PLAYING) {
        setState(() {
          isPlaying = true;
        });
      } else {
        if (mounted) {
          setState(() {
            isPlaying = false;
          });
        }
      }
    });
    //time = await audioPlayer.getDuration();
    startPlaying(widget.srcid);
  }

  @override
  void dispose() async {
    super.dispose();
    //await audioPlayer.release();
    await audioPlayer.dispose();
    audioPlayer = null;
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: LinearProgressIndicator(
        value: progress,
      ),
    );
  }
}
