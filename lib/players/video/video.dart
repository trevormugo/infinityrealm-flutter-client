import 'package:flutter/material.dart';
import './fullscreenvideo.dart';
import '../playermanager.dart';
import './actuallvideotwo.dart';

class VideoPlayerFoot extends StatefulWidget {
  VideoPlayerFoot({
    @required this.videoname,
  });
  final String videoname;
  State<StatefulWidget> createState() {
    return _VideoPlayer();
  }
}

class _VideoPlayer extends State<VideoPlayerFoot>
    with TickerProviderStateMixin {
  AnimatedIconData icon = AnimatedIcons.pause_play;
  AnimationController _iconanimate;
  void _animatepauseplay() {
    if (!VideoAppState.playerController.value.isPlaying) {
      _iconanimate.reverse();
      VideoAppState.playerController.play();
    } else {
      _iconanimate.forward();
      VideoAppState.playerController.pause();
    }
  }

  void initState() {
    super.initState();
    _iconanimate = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 500),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _iconanimate?.dispose();
  }

  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Text(widget.videoname,
            style: TextStyle(
                fontSize: 14, color: Colors.black38, fontFamily: 'NanumGothic'),
            textAlign: TextAlign.center),
        GestureDetector(
          onTap: _animatepauseplay,
          child: AnimatedIcon(
            icon: icon,
            progress: _iconanimate,
            color: Colors.black38,
          ),
        ),
      ],
    );
  }
}
