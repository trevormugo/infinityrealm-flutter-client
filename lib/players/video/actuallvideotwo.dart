import 'package:video_player/video_player.dart';
import 'package:flutter/material.dart';
import '../../ip.dart';

class VideoApp extends StatefulWidget {
  VideoApp({@required this.srcid});
  final String srcid;
  @override
  State<StatefulWidget> createState() => VideoAppState();
}

class VideoAppState extends State<VideoApp> {
  static VideoPlayerController playerController;
  static VoidCallback listener;

  @override
  void initState() {
    super.initState();
    createVideo(Adress.myip + "/uploads/streamuploadfile/" + widget.srcid);
    listener = () {
      setState(() {});
    };
  }

  static void createVideo(String route) {
    playerController = VideoPlayerController.network(route)
      ..addListener(listener)
      ..setVolume(1.0)
      ..initialize()
      ..play();
  }

  static void updateplayer(String route) {
    playerController.dispose();
    playerController = null;
    createVideo(route);
  }

  @override
  void dispose() {
    super.dispose();
    playerController.dispose();
    playerController = null;
    listener = null;
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: AspectRatio(
        aspectRatio: 16 / 9,
        child: VideoPlayer(playerController),
      ),
    );
  }
}
