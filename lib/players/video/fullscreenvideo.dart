import 'package:flutter/material.dart';
import 'dart:ui';
import 'package:line_icons/line_icons.dart';
import 'package:video_player/video_player.dart';
import '../../animations/iconanimation.dart';
import './actuallvideotwo.dart';
import '../playermanager.dart';
import '../retrievetext.dart';

class FullScreenVideoPlayer extends StatefulWidget {
  FullScreenVideoPlayer({
    @required this.songname,
    @required this.id,
    this.views,
    @required this.screenwidth,
    @required this.screenheight,
  });
  final String songname;
  final String id;
  final int views;
  final double screenwidth;
  final double screenheight;
  @override
  State<StatefulWidget> createState() {
    return _FullVideoPlayer();
  }
}

class _FullVideoPlayer extends State<FullScreenVideoPlayer> {
  bool _visible;

  bool isplaying = VideoAppState.playerController.value.isPlaying;
  void _toggle() {
    if (!VideoAppState.playerController.value.isPlaying) {
      PlayerManager().play("video");
      _visible = true;
    } else {
      PlayerManager().pause("video");
      _visible = false;
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: AlignmentDirectional.center,
      fit: StackFit.expand,
      children: <Widget>[
        SafeArea(
          top: true,
          bottom: true,
          child: Align(
            alignment: Alignment.topLeft,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(5),
                  margin: EdgeInsets.only(
                    bottom: 10,
                  ),
                  color: Colors.black.withOpacity(0.5),
                  child: Text(
                    "Music Name",
                    softWrap: true,
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(5),
                  color: Colors.black.withOpacity(0.5),
                  child: Text(
                    "Artist Name",
                    softWrap: true,
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                ),
              ],
            ),
          ),
        ),
        Container(
          decoration: BoxDecoration(
            color: Colors.white,
            gradient: LinearGradient(
              begin: FractionalOffset.topCenter,
              end: FractionalOffset.bottomCenter,
              colors: [
                Colors.transparent,
                Colors.black.withOpacity(0.9),
              ],
              stops: [0.0, 1.0],
            ),
          ),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                /*child: VideoProgressIndicator(VideoAppState.playerController,
                    allowScrubbing: true),*/ 
              ),
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      IconAnimation(
                          color: Colors.green,
                          icon: LineIcons.headphones,
                          shouldanimate: false),
                      RetrieveText(
                        texttoshow: "views",
                        uploadid: widget.id,
                      ),
                    ],
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      IconAnimation(
                          color: Colors.red,
                          icon: LineIcons.heart,
                          shouldanimate: true),
                      RetrieveText(
                        texttoshow: "views",
                        uploadid: widget.id,
                      ),
                    ],
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      IconAnimation(
                          color: Colors.orange,
                          icon: LineIcons.comments,
                          shouldanimate: true),
                      RetrieveText(
                        texttoshow: "views",
                        uploadid: widget.id,
                      ),
                    ],
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      IconAnimation(
                          color: Colors.white,
                          icon: LineIcons.share,
                          shouldanimate: true),
                      RetrieveText(
                        texttoshow: "views",
                        uploadid: widget.id,
                      ),
                    ],
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Icon(LineIcons.bars, color: Colors.white, size: 25),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
        GestureDetector(
          onTap: _toggle,
          child: AnimatedContainer(
            duration: Duration(milliseconds: 500),
            height:
                _visible ? widget.screenheight * (3 / 10) : widget.screenheight,
            width: widget.screenwidth,
            child: AnimatedOpacity(
              opacity: _visible ? 0.0 : 1.0,
              duration: Duration(milliseconds: 500),
              child: BackdropFilter(
                filter: ImageFilter.blur(
                  sigmaX: 9.0,
                  sigmaY: 9.0,
                ),
                child: Center(
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        LineIcons.angle_left,
                        color: Colors.white,
                        size: 40.0,
                      ),
                      Icon(
                        LineIcons.play,
                        color: Colors.white,
                        size: 40.0,
                      ),
                      Icon(
                        LineIcons.angle_right,
                        color: Colors.white,
                        size: 40.0,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
