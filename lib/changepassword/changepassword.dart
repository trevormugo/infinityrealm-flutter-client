import 'package:fancy_dialog/FancyGif.dart';
import 'package:flutter/material.dart';

import '../appbar.dart';
import '../customdialog.dart';
import '../main.dart';
import '../restapi.dart';
import '../animations/backgroundanimation.dart';

class PasswordChange extends StatefulWidget {
  PasswordChange({@required this.email});
  final String email;
  @override
  _PasswordChangeInstance createState() => _PasswordChangeInstance();
}

class _PasswordChangeInstance extends State<PasswordChange> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _newpasswordcontroller = TextEditingController();
  TextEditingController _repeatpasswordcontroller = TextEditingController();
  Map<String, String> resetpassreq;
  void _changepassword() async {
    if (_formKey.currentState.validate()) {
      if (_repeatpasswordcontroller.text == _newpasswordcontroller.text) {
        resetpassreq = {
          "Email": widget.email,
          "Password": _newpasswordcontroller.text
        };
        var response = await RestApi()
            .resetpassword("/useraccounts/resetpassword", resetpassreq);
        if (response.statusCode == 200) {
          showDialog(
            context: context,
            builder: (BuildContext context) => CustomDialog(
              description: "Successfuly changed password",
              gifpath: FancyGif.CHECK_MAIL,
              title: "Success",
            ),
          );
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => LoginPage(),
            ),
          );
        } else if (response.statusCode == 400) {
          showDialog(
            context: context,
            builder: (BuildContext context) => CustomDialog(
                description: "Account doesnt exist",
                gifpath: FancyGif.CHECK_MAIL,
                title: "Error"),
          );
        } else {
          showDialog(
            context: context,
            builder: (BuildContext context) => CustomDialog(
                description: "Server Error",
                gifpath: FancyGif.CHECK_MAIL,
                title: "Error"),
          );
        }
      } else {
        showDialog(
          context: context,
          builder: (BuildContext context) => CustomDialog(
              description: "Passwords dont match",
              gifpath: FancyGif.CHECK_MAIL,
              title: "Error"),
        );
      }
    } else {
      showDialog(
        context: context,
        builder: (BuildContext context) => CustomDialog(
            description: "please provide input",
            gifpath: FancyGif.CHECK_MAIL,
            title: "Input reauired"),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBarOnly(
          titletext: "Change Password",
        ),
      ),
      extendBodyBehindAppBar: true,
      body: SafeArea(
        top: true,
        bottom: false,
        left: false,
        right: false,
        child: LayoutBuilder(
          builder: (context, constraints) {
            return SingleChildScrollView(
              child: ConstrainedBox(
                constraints: BoxConstraints(
                    minWidth: constraints.maxWidth,
                    minHeight: constraints.maxHeight),
                child: Stack(
                  children: [
                    BackgroundAnimation(
                      nooficons: 15,
                      width: constraints.maxWidth,
                      height: constraints.maxHeight,
                      shouldstart: true,
                    ),
                    Container(
                      width: constraints.maxWidth,
                      height: constraints.maxHeight,
                      color: Colors.black38.withOpacity(0.4),
                    ),
                    Form(
                      key: _formKey,
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            width: double.infinity,
                            height: 50,
                            margin: EdgeInsets.all(20),
                            child: TextFormField(
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Please enter some text';
                                }
                                return null;
                              },
                              controller: _repeatpasswordcontroller,
                              obscureText: false,
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.only(
                                      bottomLeft: Radius.elliptical(30, 30),
                                      topRight: Radius.elliptical(30, 30),
                                    ),
                                  ),
                                  labelText: "Enter you\'re new password"),
                            ),
                          ),
                          Container(
                            width: double.infinity,
                            height: 50,
                            margin: EdgeInsets.all(20),
                            child: TextFormField(
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Please enter some text';
                                }
                                return null;
                              },
                              controller: _newpasswordcontroller,
                              obscureText: false,
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.only(
                                      bottomLeft: Radius.elliptical(30, 30),
                                      topRight: Radius.elliptical(30, 30),
                                    ),
                                  ),
                                  labelText: "Repeat new password"),
                            ),
                          ),
                          FlatButton(
                            onPressed: _changepassword,
                            child: Text(
                              "Change Password",
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
